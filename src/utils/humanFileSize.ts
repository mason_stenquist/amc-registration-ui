export function humanFileSize(size: number) {
  var i = Math.floor(Math.log(size) / Math.log(1024));
  var computedSize: number = parseInt((size / Math.pow(1024, i)).toFixed(2));
  return computedSize * 1 + " " + ["B", "kB", "MB", "GB", "TB"][i];
}
