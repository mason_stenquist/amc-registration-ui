import { isEmptyArray } from "formik";
import dayjs from "./dayJS";

export const isFmDate = (date?: string) => {
  if (!date) return false;
  const [m, d, y] = date.split("/");
  if (m && d && y.length === 4) {
    return true;
  }
  return false;
};

export const isJsDate = (date?: string) => {
  if (!date) return false;
  const [y, m, d] = date.split("-");
  if (m && d && y.length === 4) {
    return true;
  }
  return false;
};

export const dateConvertFmToJs = (date?: string) => {
  if (!date) return;
  const [m, d, y] = date.split("/");
  if (m && d && y) {
    return `${y}-${m}-${d}`;
  }
  return date;
};

export const dateConvertJsToFm = (date?: string) => {
  if (!date || date === "?") return;
  const [y, m, d] = date.split("-");
  if (m && d && y) {
    return `${m}/${d}/${y}`;
  }
  return date;
};

export const getDayjsDateFromFMDate = (date: string) => {
  return dayjs(date, "MM/DD/YYYY");
};

export const getDayjsDateFromFMTimestamp = (timestamp: string) => {
  if (!timestamp || timestamp === "?") return;
  return dayjs(timestamp, "MM/DD/YYYY hh:mm:ss");
};

export const hasTimeConflict = (
  aStart: dayjs.Dayjs,
  aEnd: dayjs.Dayjs,
  bStart: dayjs.Dayjs,
  bEnd: dayjs.Dayjs
) => {
  if (!aStart || !aEnd || !bStart || !bEnd) return false;
  return aStart.isSameOrBefore(bEnd) && aEnd.isSameOrAfter(bStart);
};

// export const hasTimeConflict = (
//   aStart: dayjs.Dayjs,
//   aEnd: dayjs.Dayjs,
//   bStart: dayjs.Dayjs,
//   bEnd: dayjs.Dayjs
// ) => {
//   if (aStart < bEnd && bStart < aEnd) return true;
//   if (aStart == bStart) return true; // a starts same time as b
//   if (aStart <= bStart && bStart <= aEnd) return true; // b starts in a
//   if (aStart <= bEnd && bEnd <= aEnd) return true; // b ends during a
//   if (bStart < aStart && aEnd < bEnd) return true; // a in b
//   if (bStart > aStart && bStart < aEnd) return true; // b starts during a
//   return false;
// };

export const generateDateArrayBetweenDates = (
  start: dayjs.Dayjs | undefined,
  end: dayjs.Dayjs | undefined
) => {
  const datesArray: dayjs.Dayjs[] = [];
  if (!start || !end) return datesArray;
  let date = start;
  while (date?.isSameOrBefore(end)) {
    datesArray.push(date);
    date = date.add(1, "day");
  }
  return datesArray;
};
