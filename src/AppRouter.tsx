import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import App from "./App";
import { entireRegistrationFlow } from "./modules";
import Complete from "./pages/complete/Complete";
import Home from "./pages/home/Home";
import Overview from "./pages/overview/Overview";
import Register from "./pages/register/Register";
import RegisterRedirect from "./pages/register/RegisterRedirect";
import Review from "./pages/review/Review";

const registrationModules = entireRegistrationFlow.map(({ to, element }) => ({
  path: `:guestId/${to.split("/")[3]}`,
  element,
}));

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<Home />} />
          <Route path="overview/:partyId" element={<Overview />} />
          <Route path="review/:partyId" element={<Review />} />
          <Route path="complete/:partyId" element={<Complete />} />
          <Route path="register" element={<Register />}>
            <Route path=":guestId" element={<RegisterRedirect />} />
            {registrationModules.map(({ path, element }) => (
              <Route key={path} path={path} element={element} />
            ))}
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default AppRouter;
