import { useQuery } from "react-query";
import { BASE_URL } from "../const/const";

const useAuth = (id?: string, email?: string) => {
  const doFetch = async () => {
    let res;
    try {
      const emailString = atob(email || "");
      res = await fetch(`${BASE_URL}/login.php?id=${id}&email=${emailString}`);
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Could not login";
    return await res.json();
  };
  const query = useQuery<unknown, Error>(["auth", id, email], doFetch, {
    enabled: !!id && !!email,
    retry: false,
  });
  return query;
};

export default useAuth;
