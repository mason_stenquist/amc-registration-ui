import useActivities from "../api/useActivities";
import { ActivitiyLayoutFields, Activity } from "../const/activityFieldMapping";
import { useMemo } from "react";
import dayjs from "../utils/dayJS";

interface ActivitiesByDateType {
  date: string;
  dayjsDate: dayjs.Dayjs;
  types: ActivitesByType[];
}

interface ActivitesByType {
  type: string;
  activities: ActivitiyLayoutFields[];
}

const useActivitiesGroupedByDateType = () => {
  const activitiesQuery = useActivities();

  const activitiesGroupedByDateAndType = useMemo(() => {
    let date = "";
    let dateIndex = -1;
    let type = "";
    let typeIndex = -1;
    const array: ActivitiesByDateType[] = [];

    (activitiesQuery.data || []).forEach((activity) => {
      //Check if we've hit a new date
      if (date !== activity[Activity.Date]) {
        date = activity[Activity.Date];
        dateIndex++;
        array.push({
          date,
          dayjsDate: activity.dayjsDate,
          types: [],
        });
        type = "";
        typeIndex = -1;
      }

      //Check if we've hit a new type
      if (type !== activity[Activity.Type]) {
        type = activity[Activity.Type];
        typeIndex++;
        array[dateIndex].types.push({ type, activities: [] });
      }

      array[dateIndex].types[typeIndex].activities.push(activity);
    });
    return array;
  }, [activitiesQuery.data]);

  return activitiesGroupedByDateAndType;
};

export default useActivitiesGroupedByDateType;
