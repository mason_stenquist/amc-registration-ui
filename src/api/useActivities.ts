import { useQuery } from "react-query";
import { ActivitiyLayoutFields, Activity } from "../const/activityFieldMapping";
import { BASE_URL } from "../const/const";
import { Registration } from "../const/guestFieldMapping";
import {
  dateConvertJsToFm,
  getDayjsDateFromFMDate,
  getDayjsDateFromFMTimestamp,
} from "../utils/dateUtils";
import useGuest from "./useGuest";

const useActivities = (guestId?: string) => {
  const guestQuery = useGuest(guestId);
  const startDate = dateConvertJsToFm(
    guestQuery.data?.[Registration.DateAirArrival]
  );
  const endDate = dateConvertJsToFm(
    guestQuery.data?.[Registration.DateAirDeparture]
  );

  const getActivities = async () => {
    let res;
    try {
      res = await fetch(
        `${BASE_URL}/getActivities.php?startDate=${startDate}&endDate=${endDate}`
      );
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Error, could not get activity data";
    const activities = await res.json();
    return activities.map((activity: ActivitiyLayoutFields) => {
      //Do some weird bullshit that was probably a bug in PHP, but actually makes the site behave correctly :-(
      const startConflictTime = getDayjsDateFromFMTimestamp(
        activity?.[Activity.TimestampStartConflict]
      );

      const endConflictTime = getDayjsDateFromFMTimestamp(
        activity?.[Activity.TimestampEndConflict]
      );

      return {
        ...activity,
        dayjsDate: getDayjsDateFromFMDate(activity[Activity.Date]),
        dayjsTimestampStart: getDayjsDateFromFMTimestamp(
          activity[Activity.TimestampStart]
        ),
        dayjsTimestampEnd: getDayjsDateFromFMTimestamp(
          activity[Activity.TimestampEnd]
        ),
        dayjsTimestampStartConflict: startConflictTime,
        dayjsTimestampEndConflict: endConflictTime,
      };
    });
  };
  return useQuery<ActivitiyLayoutFields[], Error>(
    ["activities", startDate, endDate],
    getActivities,
    { enabled: !!startDate && !!endDate }
  );
};

export default useActivities;
