import { useMutation } from "react-query";
import { BASE_URL } from "../const/const";

export type FileUploadResponse = Record<
  string,
  {
    error: null | string;
    url?: string;
  }
>;

const useFilesUpload = () => {
  const uploadFile = async (data: FormData) => {
    let res;
    try {
      res = await fetch(`${BASE_URL}/filesUpload.php`, {
        method: "POST",
        body: data,
      });
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Error, could not perform script";
    return await res.json();
  };
  return useMutation<FileUploadResponse, Error, FormData>(uploadFile);
};

export default useFilesUpload;
