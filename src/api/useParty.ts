import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { BASE_URL } from "../const/const";
import { PartyLayoutFields } from "../const/partyFieldMapping";

const useParty = (partyId?: string) => {
  const { partyId: paramPartyId } = useParams();

  const queryPartyId = partyId ?? paramPartyId;

  const getParty = async () => {
    let res;
    try {
      res = await fetch(`${BASE_URL}/getParty.php?partyId=${queryPartyId}`);
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Error, could not get guest data";
    const records = await res.json();
    return records.map((r: PartyLayoutFields) => ({ ...r, isComplete: false }));
  };
  return useQuery<PartyLayoutFields[], Error>("party", getParty, {
    enabled: !!queryPartyId,
  });
};

export default useParty;
