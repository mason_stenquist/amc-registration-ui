import { useMutation } from "react-query";
import { BASE_URL } from "../const/const";

interface Response {
  messages: { code: string; message: string }[];
  response: {
    scriptError: string;
    scriptResult: string;
  };
}

const usePeformScript = <T>(scriptName: string) => {
  const performScript = async (scriptParams: Record<string, unknown>) => {
    let res;
    try {
      res = await fetch(
        `${BASE_URL}/performScript.php?scriptName=${scriptName}`,
        {
          method: "POST",
          body: JSON.stringify(scriptParams),
        }
      );
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Error, could not perform script";
    const data = (await res.json()) as Response;
    return JSON.parse(data.response.scriptResult);
  };
  return useMutation<T, Error, Record<string, unknown>>(performScript);
};

export default usePeformScript;
