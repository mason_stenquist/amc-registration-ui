import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { BASE_URL } from "../const/const";
import { Guest, GuestLayoutFields, Wave } from "../const/guestFieldMapping";
import { getDayjsDateFromFMDate } from "../utils/dateUtils";

const useGuest = (id?: string) => {
  const { guestId, partyId } = useParams();

  const queryGuestId = id ?? guestId ?? partyId;

  const getGuest = async () => {
    let res;
    try {
      res = await fetch(`${BASE_URL}/getGuest.php?id=${queryGuestId}`);
    } catch (e) {
      console.log(e);
      throw e;
    }
    if (!res.ok) throw "Error, could not get guest data";
    const guest = await res.json();
    return {
      ...guest,
      activities: JSON.parse(guest[Guest.zctActivityIds]),
      [Guest.CovidPhotoFront]: guest?.covidPhotoFrontJSON
        ? [JSON.parse(guest?.covidPhotoFrontJSON)]
        : [],
      [Guest.CovidPhotoBack]: guest?.covidPhotoBackJSON
        ? [JSON.parse(guest?.covidPhotoBackJSON)]
        : [],
      dayjsArrivalStart: getDayjsDateFromFMDate(guest[Wave.ArrivalStartDate]),
      dayjsArrivalEnd: getDayjsDateFromFMDate(guest[Wave.ArrivalEndDate]),
      dayjsDepatureStart: getDayjsDateFromFMDate(
        guest[Wave.DepartureStartDate]
      ),
      dayjsDepatureEnd: getDayjsDateFromFMDate(guest[Wave.DepartureEndDate]),
    };
  };
  return useQuery<GuestLayoutFields, Error>(["guest", queryGuestId], getGuest);
};

export default useGuest;
