export type PartyLayoutFields = Record<Party, string> & {
  isComplete: false;
};

export enum Party {
  CompGroup = "CompOrg_Group",
  CompName = "CompOrg_Name",
  CompPosition = "CompOrg_Position",
  Email = "Contact_eMail",
  NameFirst = "Name_First",
  NameLast = "Name_Last",
  PartyStatus = "Party_Status",
  Phone = "Contact_Phone",
  zctActivityIds = "zctActivityIdArray",
  ZPartyID = "z_PartyID",
  Zuuid = "z___UUID",
}
