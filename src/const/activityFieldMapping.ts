import dayjs from "../utils/dayJS";

export type ActivitiyLayoutFields = Record<Activity, string> &
  Record<ActivityNum, number> & {
    dayjsDate: dayjs.Dayjs;
    dayjsTimestampStart: dayjs.Dayjs;
    dayjsTimestampEnd: dayjs.Dayjs;
    dayjsTimestampStartConflict: dayjs.Dayjs;
    dayjsTimestampEndConflict: dayjs.Dayjs;
  };

export enum Activity {
  Activity = "Activity",
  Category = "Category",
  Date = "Date",
  Description = "Description",
  GroupActivity = "GroupActivity",
  MassageConflict = "Massage_Conflict",
  PrefGolf = "require_golfClubPreference",
  PrefMassage = "require_massage",
  SessionCode = "Session_Code",
  TimeEnd = "Time_End",
  TimestampEnd = "TimeStamp_End",
  TimestampEndConflict = "TimeStamp_End_Conflict",
  TimestampStart = "TimeStamp_Start",
  TimestampStartConflict = "TimeStamp_Start_Conflict",
  TimeStart = "Time_Start",
  Type = "Type",
  TypeScort = "Type_Sort",
  WebRegVisibility = "WebReg_Visibility",
  Zuuid = "z___UUID",
}

export enum ActivityNum {
  CountRemaining = "Count_Remaining",
}
