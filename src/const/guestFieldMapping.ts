import dayjs from "../utils/dayJS";

interface Photo {
  error: null | string;
  url: string;
  id: string;
  fieldName: string;
  name: string;
  size: number;
}

export type GuestLayoutFields = Record<
  Guest | Activity | Registration | Wave,
  string
> & {
  activities: string[];
  [Guest.CovidPhotoFront]: Photo[];
  [Guest.CovidPhotoBack]: Photo[];
  dayjsArrivalStart: dayjs.Dayjs;
  dayjsArrivalEnd: dayjs.Dayjs;
  dayjsDepatureStart: dayjs.Dayjs;
  dayjsDepatureEnd: dayjs.Dayjs;
};

export enum Guest {
  AccountPassword = "Account_Password",
  AccountUsername = "Account_Username",
  AddressCity = "Add_City",
  AddressCountry = "Add_Country",
  AddressSecondCity = "Second_Add_City",
  AddressSecondState = "Second_Add_State",
  AddressSecondStreet = "Second_Add_Street",
  AddressSecondZip = "Second_Add_ZipPost",
  AddressState = "Add_State",
  AddressStreet = "Add_Street",
  AddressZip = "Add_ZipPost",

  CompGroup = "CompOrg_Group",
  CompName = "CompOrg_Name",
  CompPosition = "CompOrg_Position",
  CovidPhotoBack = "covidPhotoBack",
  CovidPhotoBackJson = "covidPhotoBackJSON",
  CovidPhotoBooster = "covidPhotoBooster",
  CovidPhotoBoosterJson = "covidPhotoBoosterJSON",
  CovidPhotoFront = "covidPhotoFront",
  CovidPhotoFrontJson = "covidPhotoFrontJSON",
  CovidWaiver = "covidWaiver",

  Dob = "DOB_Date",
  Email = "Contact_eMail",
  EmergencyEmail = "Emerg_eMail",
  EmergencyName = "Emerg_Name",
  EmergencyPhone = "Emerg_Phone",
  FreqFlyerCarrier = "FreqF_Carier",
  FreqFlyerNo = "FreqF_No",
  Gender = "Gender",
  GolfRentalPref = "GolfRental_Preference",
  GuestPhoto = "Guest_Photo",
  InvoiceTotal = "Invoice_Total",
  MassagePref = "Massage_Preference",
  MaxWaveDate = "zcdMaxWaveDate",
  MinWaveDate = "zcdMinWaveDate",
  NameFirst = "Name_First",
  NameFirstLast = "Name__FirstLast",
  NameLast = "Name_Last",
  NameMid = "Name_Mid",
  NameTitle = "Name_Title",
  PartyGuestsAll = "Party_GuestsALL",
  PartyInvoiceTotal = "Party_Invoice_Total",
  PartyStatus = "Party_Status",
  PassportExpDate = "Pas_DateExp",
  PassportName = "Pas_Name",
  PassportNationality = "Pas_Nationality",
  PassportNumber = "Pas_Number",
  Phone = "Contact_Phone",
  Photo = "Photo",
  SkiAbility = "Ski_Ability",
  SpecialNeeds = "SpecialNeeds",
  zctActivityIds = "zctActivityIdArray",
  ZPartyID = "z_PartyID",
  Zuuid = "z___UUID",
}

export enum Activity {
  Category = "ACTIVITY::Category",
  Date = "ACTIVITY::Date",
  Start = "ACTIVITY::Time_Start",
  Activity = "ACTIVITY::Activity",
  Reserved = "ACTIVITY::Count_Reserved",
  Remaining = "ACTIVITY::Count_Remaining",
  Zuuid = "ACTIVITY::z___UUID",
  End = "ACTIVITY::Time_End",
  SessionCode = "ACTIVITY::Session_Code",
}

export enum Registration {
  AirportCodeArrival = "guest_REGISTRATION::AirportCode_Arrival",
  AirportCodeDeparture = "guest_REGISTRATION::AirportCode_Departure",
  ArrivalAirline = "guest_REGISTRATION::Arrival_FlightAirline",
  ArrivalFlightNo = "guest_REGISTRATION::Arrival_FlightNo",
  ArrivalTailNumber = "guest_REGISTRATION::Arrival_Tail_Number",
  AssistantEmail = "guest_REGISTRATION::Assistant_eMail",
  AssistantName = "guest_REGISTRATION::Assistant_Name",
  CompletedByName = "guest_REGISTRATION::Completed_ByName",
  CompletedEmail = "guest_REGISTRATION::Completed_eMail",
  CompletedOther = "guest_REGISTRATION::Completed_Other",
  CompletedPhone = "guest_REGISTRATION::Completed_Phone",
  DateAirArrival = "guest_REGISTRATION::Date_Air_Arrival",
  DateAirDeparture = "guest_REGISTRATION::Date_Air_Departure",
  DateHousingArrival = "guest_REGISTRATION::Date_Housing_Arrival",
  DeateHousingDeparture = "guest_REGISTRATION::Date_Housing_Departure",
  DepartureTailNumber = "guest_REGISTRATION::Departure_Tail_Number",
  DepatureAirline = "guest_REGISTRATION::Departure_FlightAirline",
  DepatureFlightNo = "guest_REGISTRATION::Depature_FlightNo",
  Email = "guest_REGISTRATION::eMail",
  GuestType = "guest_REGISTRATION::Guest_Type",
  HousingBedType = "guest_REGISTRATION::Housing_BedType",
  International = "guest_REGISTRATION::International",
  NameFirst = "guest_REGISTRATION::Name_First",
  NameLast = "guest_REGISTRATION::Name_Last",
  NeedAir = "guest_REGISTRATION::Need_Air",
  NeedAirReason = "guest_REGISTRATION::Need_Air_Reason",
  NeedGround = "guest_REGISTRATION::Need_Ground",
  NeedGroundReason = "guest_REGISTRATION::Need_Ground_Reason",
  NeedHotel = "guest_REGISTRATION::Need_Hotel",
  NeedHotelReason = "guest_REGISTRATION::Need_Hotel_Reason",
  RegistrationDevice = "guest_REGISTRATION::Registration_Device",
  SpecialNeedsDiet = "guest_REGISTRATION::Special_Needs_Diet",
  SpecialNeedsHousing = "guest_REGISTRATION::Special_Needs_Housing",
  SpecialNeedsTravel = "guest_REGISTRATION::Special_Needs_Travel",
  TimeRegComplete = "guest_REGISTRATION::TimeStamp_RegComplete",
  TimeSendConfirm = "guest_REGISTRATION::TimeStamp_SendConfirmation",
  TimeSendInvite = "guest_REGISTRATION::TimeStamp_SendInvitation",
  TimeSentCovidRequest = "guest_REGISTRATION::TimeStamp_SendCovidRequest",
  ZGroupUuid = "guest_REGISTRATION::z__GroupUUID",
  zGuestUuid = "guest_REGISTRATION::z__GuestUUID",
  Zuuid = "guest_REGISTRATION::z___UUID",
}

export enum Wave {
  ArrivalStartDate = "guest_registration_WAVE::arrivalStartDate",
  ArrivalEndDate = "guest_registration_WAVE::arrivalEndDate",
  DepartureStartDate = "guest_registration_WAVE::departureStartDate",
  DepartureEndDate = "guest_registration_WAVE::departureEndDate",
}
