export const BASE_URL = import.meta.env.VITE_API_BASE_URL;
export const CREATE_GUEST_SCRIPT = "php_CreateGuest";
export const SAVE_GUEST_SCRIPT = "php_SaveGuestDetails";
export const SAVE_ACTIVITIES_SCRIPT = "php_UpdateActivities";
export const DOWNLOAD_IMAGE_SCRIPT = "php_downloadImage";
export const INTERNAL_EMAIL_SCRIPT = "WEB_registration_SendCompleteEmail";
export const CONFIRMATION_EMAIL_SCRIPT =
  "server_SEND_CONFIRMATION_GUEST_email_(GuestID)";
export const DELETE_GUEST_SCRIPT = "php_DeleteGuest";

export const EVENT_AIR_CODE = "PHX";
export const DEFAULT_WAVE = "Wave 1";
export enum TextBool {
  Yes = "Yes",
  No = "No",
}
