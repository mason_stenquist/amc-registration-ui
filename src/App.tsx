import { Outlet, useLocation } from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";

function App() {
  let { pathname } = useLocation();

  if (pathname === "/") return <Outlet />;

  return (
    <div className="App">
      <DefaultLayout subtitle="Please complete all registration fields that apply">
        <Outlet />
      </DefaultLayout>
    </div>
  );
}

export default App;
