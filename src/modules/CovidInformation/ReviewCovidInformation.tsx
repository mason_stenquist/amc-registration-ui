import CovidModule from ".";
import useGuest from "../../api/useGuest";
import ReviewHeader from "../../components/ReviewHeader";
import ReviewRow from "../../components/ReviewRow";

interface Props {
  id: string;
}

const ReviewCovidInformation: React.FC<Props> = ({ id }) => {
  const { data: guest } = useGuest(id);

  return (
    <div className="mb-4">
      <ReviewHeader guestId={id} module={CovidModule} />
      <table className="w-full">
        <tbody>
          <ReviewRow label="Covid Front">
            {guest?.covidPhotoFront[0].name}
          </ReviewRow>
          <ReviewRow label="Covid Back">
            {guest?.covidPhotoBack[0].name}
          </ReviewRow>
        </tbody>
      </table>
    </div>
  );
};

export default ReviewCovidInformation;
