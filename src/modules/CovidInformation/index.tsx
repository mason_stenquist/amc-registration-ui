import { Form, Formik } from "formik";
import useGuest from "../../api/useGuest";
import { getNextModule, Module } from "../../modules";
import FileUpload from "../../components/FileUpload";
import { Guest, Registration } from "../../const/guestFieldMapping";
import NextPrevNavigation from "../../components/NextPrevNavigation";
import * as Yup from "yup";
import useFilesUpload, { FileUploadResponse } from "../../api/useFilesUpload";
import { toast } from "react-toastify";
import usePeformScript from "../../api/usePerformScript";
import { DOWNLOAD_IMAGE_SCRIPT } from "../../const/const";
import { useQueryClient } from "react-query";
import { useMemo } from "react";
import ReviewCovidInformation from "./ReviewCovidInformation";
import useGoToNext from "../hooks/useGoToNext";
import { generatePath, useNavigate, useParams } from "react-router-dom";

const CovidInformationSchema = Yup.object().shape({
  [Guest.CovidPhotoFront]: Yup.array()
    .length(1, "Required")
    .required("Required"),
  [Guest.CovidPhotoBack]: Yup.array()
    .length(1, "Required")
    .required("Required"),
});

const CovidInformation: React.FC = () => {
  const queryClient = useQueryClient();
  const guestQuery = useGuest();
  const fileUpload = useFilesUpload();
  const downloadImage = usePeformScript(DOWNLOAD_IMAGE_SCRIPT);
  const nextModule = getNextModule(guestQuery?.data, CovidModule);
  const next = useGoToNext(CovidModule);
  const navigate = useNavigate();
  const params = useParams();

  const mutationStatus = useMemo(() => {
    if (fileUpload.isError || downloadImage.isError) return "error";
    if (fileUpload.isLoading || downloadImage.isLoading) return "loading";
    return "idle";
  }, [fileUpload, downloadImage]);

  const compileFormData = (values: any) => {
    const data = new FormData();
    //Add Front Photo
    const covidFrontFile = values[Guest.CovidPhotoFront][0] as File;
    data.append(Guest.CovidPhotoFront, covidFrontFile, covidFrontFile.name);
    //Add Back Photo
    const covidBackPhoto = values[Guest.CovidPhotoBack][0] as File;
    data.append(Guest.CovidPhotoBack, covidBackPhoto, covidBackPhoto.name);
    return data;
  };

  const saveToFm = async (data: FileUploadResponse, values: any) => {
    await downloadImage.mutateAsync({
      ...data[Guest.CovidPhotoFront],
      id: guestQuery.data?.[Guest.Zuuid],
      fieldName: Guest.CovidPhotoFront,
      name: values[Guest.CovidPhotoFront][0].name,
      size: values[Guest.CovidPhotoFront][0].size,
    });

    await downloadImage.mutateAsync({
      ...data[Guest.CovidPhotoBack],
      id: guestQuery.data?.[Guest.Zuuid],
      fieldName: Guest.CovidPhotoBack,
      name: values[Guest.CovidPhotoBack][0].name,
      size: values[Guest.CovidPhotoBack][0].size,
    });

    // queryClient.invalidateQueries(["guest", guestQuery.data?.[Guest.Zuuid]]);
    // guestQuery.refetch();
    // next();
    guestQuery.refetch();
    if (nextModule) {
      navigate(generatePath(nextModule.to, params));
    } else {
      navigate(
        generatePath("/overview/:partyId", {
          partyId: guestQuery?.data?.[Guest.ZPartyID],
        })
      );
    }
  };

  if (guestQuery.isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <Formik
      initialValues={{
        [Guest.CovidPhotoFront]: guestQuery.data?.[Guest.CovidPhotoFront] ?? [],
        [Guest.CovidPhotoBack]: guestQuery.data?.[Guest.CovidPhotoBack] ?? [],
      }}
      validationSchema={CovidInformationSchema}
      onSubmit={(values) => {
        console.log(values);
        fileUpload.mutate(compileFormData(values), {
          onError: (e) => {
            console.log(e);
            toast.error("Error: Could not upload files");
          },
          onSuccess: (data) => {
            console.log(data);
            saveToFm(data, values);
          },
        });
      }}
    >
      <Form className="lg:flex">
        <div className="w-full">
          <h2 className="text-lg font-bold">{CovidModule?.title}</h2>
          <div className="space-y-6">
            <FileUpload
              label="Covid Vaccination Card Front Image"
              name={Guest.CovidPhotoFront}
            />
            <FileUpload
              label="Covid Vaccination Card Back Image"
              name={Guest.CovidPhotoBack}
            />
          </div>
          <NextPrevNavigation
            currentModule={CovidModule}
            mutationStatus={mutationStatus}
          />
        </div>
      </Form>
    </Formik>
  );
};

export const CovidModule: Module = {
  to: "/register/:guestId/covid-information",
  title: "Covid Information",
  element: <CovidInformation />,
  review: (id) => <ReviewCovidInformation id={id} />,
  schema: CovidInformationSchema,
  showModule: (guest) =>
    guest?.[Registration.GuestType].toLowerCase() !== "staff",
};

export default CovidModule;
