import useGuest from "../../api/useGuest";
import * as Yup from "yup";
import { useMemo } from "react";

const useValidationCheck = (
  id: string | undefined,
  schema: Yup.ObjectSchema<any>
) => {
  const guestQuery = useGuest(id);

  const isValid = useMemo(() => {
    if (!id) return false;
    return schema.isValidSync(guestQuery.data);
  }, [id, guestQuery.data, schema]);

  return isValid;
};

export default useValidationCheck;
