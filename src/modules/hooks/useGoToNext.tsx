import useGuest from "../../api/useGuest";
import { getNextModule, Module } from "..";
import { Guest } from "../../const/guestFieldMapping";
import {
  generatePath,
  useNavigate,
  useParams,
  useSearchParams,
} from "react-router-dom";
import { useMemo } from "react";

const useGoToNext = (currentModule: Module) => {
  const guestQuery = useGuest();
  const navigate = useNavigate();
  const params = useParams();
  const [searchParams] = useSearchParams();
  const nextModule = getNextModule(guestQuery?.data, currentModule);

  const url = useMemo(() => {
    if (searchParams.get("return")) return searchParams.get("return") ?? "";
    if (nextModule) return generatePath(nextModule.to, params);
    return generatePath("/overview/:partyId", {
      partyId: guestQuery?.data?.[Guest.ZPartyID],
    });
  }, [nextModule, searchParams]);

  const next = () => {
    navigate(url);
  };

  return next;
};

export default useGoToNext;
