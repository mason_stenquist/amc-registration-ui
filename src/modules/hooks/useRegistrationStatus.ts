import useGuest from "../../api/useGuest";
import { useEffect, useMemo } from "react";
import { getGuestFlow } from "../index";
import { useQueryClient } from "react-query";
import { Party } from "../../const/partyFieldMapping";
import useParty from "../../api/useParty";

const useRegistrationStatus = (id: string | undefined) => {
  const guestQuery = useGuest(id);
  const partyQuery = useParty();
  const queryClient = useQueryClient();
  const flow = getGuestFlow(guestQuery.data);

  const updatePartyCompleteStatus = (isComplete: boolean) => {
    const updatedPartyArray = (partyQuery.data || []).map((p) =>
      p[Party.Zuuid] === id ? { ...p, isComplete } : p
    );
    queryClient.setQueryData("party", updatedPartyArray);
  };

  //Track each section of the application
  const modulesStatus = useMemo(() => {
    return flow.map(({ title, schema, to }) => ({
      title,
      to,
      isValid: !id ? false : schema.isValidSync(guestQuery.data),
    }));
  }, [id, guestQuery.data, flow]);

  //Track if the entire application has been completed
  useEffect(() => {
    const isComplete =
      modulesStatus.length === modulesStatus.filter((m) => m.isValid).length;

    const partyRecord = (partyQuery.data || []).find(
      (p) => p[Party.Zuuid] === id
    );

    if (partyRecord && partyRecord?.isComplete !== isComplete) {
      updatePartyCompleteStatus(isComplete);
    }
  }, [modulesStatus]);

  return modulesStatus;
};

export default useRegistrationStatus;
