interface Props {
  type: string;
}
const ActivityHeader: React.FC<Props> = ({ type }) => (
  <thead>
    <tr>
      <th className="sticky top-[36px] min-w-[24px] bg-slate-200 p-1 text-sm text-brand"></th>
      <th className="sticky top-[36px] min-w-[70px] bg-slate-200 p-1 text-right text-sm text-brand">
        Start
      </th>
      <th className="sticky top-[36px] min-w-[70px] bg-slate-200 p-1 text-right text-sm text-brand">
        End
      </th>
      <th className="sticky top-[36px] w-full bg-slate-200 p-1 pl-4 text-sm text-brand">
        {type}
      </th>
    </tr>
  </thead>
);

export default ActivityHeader;
