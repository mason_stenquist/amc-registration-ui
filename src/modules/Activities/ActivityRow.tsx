import classNames from "classnames";
import { Field, useField } from "formik";
import { useEffect, useMemo, useState } from "react";
import {
  ActivitiyLayoutFields,
  Activity,
  ActivityNum,
} from "../../const/activityFieldMapping";
import { hasTimeConflict } from "../../utils/dateUtils";
import useSelectedActivities from "./useSelectedActivities";

interface Props {
  activity: ActivitiyLayoutFields;
}

const ActivityRow: React.FC<Props> = ({ activity }) => {
  const [show, setShow] = useState(false);
  const [remaining, setRemaining] = useState(0);
  const [startedSelected, setStartedSelected] = useState(false);
  const toggleShow = () => setShow(!show);
  const [field, meta, helpers] = useField<string[]>("activities");
  const { setTouched } = helpers;
  const selectedActivities = useSelectedActivities();

  const isSelected = useMemo(() => {
    return field.value.includes(activity[Activity.Zuuid]);
  }, [field.value, activity]);

  useEffect(() => {
    let starting = activity[ActivityNum.CountRemaining];
    setRemaining(starting);
    setStartedSelected(isSelected);
  }, [activity[ActivityNum.CountRemaining]]);

  useEffect(() => {
    if (startedSelected) {
      setRemaining((old) => (isSelected ? old - 1 : old + 1));
    }
  }, [isSelected]);

  const canSelect = useMemo(() => {
    //If nothing selected yet, you can select anything
    if (field.value.length === 0) return true;

    //If the current row is selected, allow user to unselect
    if (field.value.includes(activity[Activity.Zuuid])) return true;

    //Only allow one massage per day
    if (
      activity[Activity.MassageConflict] &&
      selectedActivities.some(
        (act) =>
          act[Activity.Date] === activity[Activity.Date] &&
          act[Activity.MassageConflict]
      )
    ) {
      return false;
    }

    //Check for general timeoverlap issues
    return !selectedActivities.some(
      ({ dayjsTimestampStartConflict, dayjsTimestampEndConflict }) =>
        hasTimeConflict(
          dayjsTimestampStartConflict,
          dayjsTimestampEndConflict,
          activity.dayjsTimestampStartConflict,
          activity.dayjsTimestampEndConflict
        )
    );
  }, [selectedActivities]);

  const rowClasses = classNames("text-sm align-top", {
    "bg-green-50": isSelected,
    "even:bg-slate-50": !isSelected,
    "even:bg-slate-50 text-gray-300": !canSelect,
  });

  const activityNameClasses = classNames(
    "cursor-pointer font-semibold  hover:underline",
    {
      "text-brand": canSelect,
      "text-brand/25": !canSelect,
    }
  );

  const descriptionClasses = classNames(
    "overflow-hidden transition-all duration-300",
    {
      "max-h-[0]": !show,
      "max-h-[500px]": show,
    }
  );

  if (remaining <= 0 && !isSelected) return null;

  return (
    <tr className={rowClasses}>
      <td className="items-center space-x-3 pl-1 sm:pt-[8px] sm:pl-2.5">
        <div className="pt-1 sm:pt-0">
          <Field
            type="checkbox"
            name="activities"
            className="h-5 w-5 text-2xl accent-blue-600 hover:accent-blue-700"
            value={activity[Activity.Zuuid]}
            id={activity[Activity.Zuuid]}
            onClick={() => setTouched(true)}
            disabled={!canSelect}
          />
        </div>
        <label
          htmlFor={activity[Activity.Zuuid]}
          className="block cursor-pointer font-bold hover:text-brand hover:underline sm:hidden"
        >
          {activity[Activity.Activity]}
        </label>
      </td>
      <td className="p-1 py-2 text-right ">
        <p className="block sm:hidden">Start Time:</p>
        {activity.dayjsTimestampStart.format("h:mm A")}
      </td>
      <td className="p-1 py-2 text-right">
        <p className="block sm:hidden">End Time:</p>
        {activity.dayjsTimestampEnd.format("h:mm A")}
      </td>
      <td className="hidden p-1 py-2 sm:table-cell sm:pl-4">
        <button
          type="button"
          onClick={toggleShow}
          className={activityNameClasses}
        >
          {activity[Activity.Activity]}{" "}
          {activity[Activity.Description].length > 1 && <>ⓘ</>}
        </button>
        <p className={descriptionClasses}>{activity[Activity.Description]}</p>
      </td>
    </tr>
  );
};

export default ActivityRow;
