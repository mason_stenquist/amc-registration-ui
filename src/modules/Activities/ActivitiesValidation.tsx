import { useField } from "formik";

const ActivitiesValidation = () => {
  const [_field, meta, _helpers] = useField<boolean>("activities");

  if (!meta.error || !meta.touched) return null;

  return <div className="py-2 text-red-600">{meta.error}</div>;
};

export default ActivitiesValidation;
