import { Form, Formik } from "formik";
import useGuest from "../../api/useGuest";
import { getNextModule, Module } from "../";
import NextPrevNavigation from "../../components/NextPrevNavigation";
import * as Yup from "yup";
import { Guest, Registration } from "../../const/guestFieldMapping";
import { useQueryClient } from "react-query";
import usePeformScript from "../../api/usePerformScript";
import { generatePath, Link, useNavigate, useParams } from "react-router-dom";
import { SAVE_ACTIVITIES_SCRIPT, SAVE_GUEST_SCRIPT } from "../../const/const";
import { toast } from "react-toastify";
import { Activity } from "../../const/activityFieldMapping";
import useActivitiesGroupedByDateType from "../../api/useActivitiesGroupedByDateType";
import ActivityRow from "./ActivityRow";
import ActivityHeader from "./ActivityHeader";
import TripInformationModule from "../TripInformation";
import GolfRentalPreference from "./GolfRentalPreference";
import { ActivitySchema, getInitialValues } from "./activityFields";
import MassagePreference from "./MassagePreference";
import ReviewActivities from "./ReviewActivities";
import { dateConvertJsToFm } from "../../utils/dateUtils";
import ActivityMassUpdater from "./ActivityMassUpdater";
import ActivitiesValidation from "./ActivitiesValidation";

const Activities: React.FC = () => {
  const guestQuery = useGuest();
  const queryClient = useQueryClient();
  const activiesByDateType = useActivitiesGroupedByDateType();
  const saveActivities = usePeformScript(SAVE_ACTIVITIES_SCRIPT);
  const navigate = useNavigate();
  const params = useParams();

  const nextModule = getNextModule(guestQuery?.data, ActivitiesModule);

  if (
    !guestQuery.isLoading &&
    (!dateConvertJsToFm(guestQuery?.data?.[Registration.DateAirArrival]) ||
      !dateConvertJsToFm(guestQuery?.data?.[Registration.DateAirDeparture]))
  ) {
    return (
      <div className="rounded border border-yellow-500 bg-yellow-50 p-6 text-yellow-900">
        Please go back and select your arrival/depature date in the{" "}
        <Link
          to={generatePath(TripInformationModule.to, params)}
          className="text-black underline hover:text-brand"
        >
          Trip Information
        </Link>{" "}
        section before selecting activities
      </div>
    );
  }

  if (guestQuery.isLoading || activiesByDateType.length === 0) {
    return <div>Loading Activities...</div>;
  }

  return (
    <Formik
      validationSchema={ActivitySchema}
      initialValues={getInitialValues(guestQuery?.data)}
      onSubmit={(values) => {
        saveActivities.mutate(values, {
          onError: (err) => {
            console.log(err);
            toast.error("Error: Could not update agenda");
          },
          onSuccess: (data) => {
            queryClient.invalidateQueries(["guest", params?.guestId]);
            queryClient.invalidateQueries("party");
            if (nextModule) {
              navigate(generatePath(nextModule.to, params));
            } else {
              navigate(
                generatePath("/overview/:partyId", {
                  partyId: guestQuery?.data?.[Guest.ZPartyID],
                })
              );
            }
          },
        });
      }}
    >
      <Form className="lg:flex">
        <div className="w-full">
          <h2 className="text-lg font-bold">{ActivitiesModule?.title}</h2>
          {guestQuery.data?.[Registration.GuestType].toLowerCase() !==
            "staff" && (
            <p className="">
              You can select <strong>multiple activities</strong> in one day but
              only ONE spa treatment per day
            </p>
          )}

          {guestQuery.data?.[Registration.GuestType].toLowerCase() ===
            "staff" && (
            <p className="">
              You can select <strong>multiple activities</strong> in one day,
              but only ONE spa treatment for duration of trip
            </p>
          )}

          <p className="py-2 text-red-600">
            Click on activity name for description and suggested attire
          </p>

          <ActivityMassUpdater />

          {activiesByDateType.map(({ date, dayjsDate, types }) => (
            <div key={date}>
              <h2 className="sticky top-[-1px] z-10 -mx-4 h-[37px] bg-brand px-2 pt-2 text-sm font-bold text-white sm:mx-0 sm:text-lg">
                {dayjsDate.format("dddd, MMMM D, YYYY")}
              </h2>
              {types.map(({ type, activities }) => (
                <table key={type} className="w-full text-left">
                  <>
                    <ActivityHeader type={type} />
                    <tbody>
                      {activities.map((activity) => (
                        <ActivityRow
                          key={activity[Activity.Zuuid]}
                          activity={activity}
                        />
                      ))}
                    </tbody>
                  </>
                </table>
              ))}
            </div>
          ))}

          <GolfRentalPreference />
          <MassagePreference />
          <ActivitiesValidation />

          <input type="hidden" name={Guest.Zuuid} />
          <NextPrevNavigation
            currentModule={ActivitiesModule}
            mutationStatus={saveActivities.status}
          />
        </div>
      </Form>
    </Formik>
  );
};

export const ActivitiesModule: Module = {
  to: "/register/:guestId/activities",
  title: "Activities",
  element: <Activities />,
  review: (id) => <ReviewActivities id={id} />,
  schema: ActivitySchema,
  showModule: (guest) => true,
};

export default ActivitiesModule;
