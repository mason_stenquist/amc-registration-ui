import { useField } from "formik";
import { useEffect, useMemo } from "react";
import Select from "../../components/Select";
import { Activity } from "../../const/activityFieldMapping";
import { Guest } from "../../const/guestFieldMapping";
import useSelectedActivities from "./useSelectedActivities";

const MassagePreference = () => {
  const [field, _meta, helpers] = useField<boolean>("hasMassage");
  const { setValue } = helpers;
  const selectedActivities = useSelectedActivities();

  const showField = useMemo(() => {
    return selectedActivities.some((act) => act?.[Activity.PrefMassage]);
  }, [selectedActivities]);

  useEffect(() => {
    if (field.value !== showField) {
      setValue(showField);
    }
  }, [showField]);

  if (!showField) return null;

  return (
    <div className="py-2">
      <p className="text-lg font-bold">Massage Preferences</p>
      <Select name={Guest.MassagePref} label="Masseuse Preference">
        <option></option>
        <option>Male</option>
        <option>Female</option>
      </Select>
    </div>
  );
};

export default MassagePreference;
