import { useField } from "formik";
import useGuest from "../../api/useGuest";
import { Guest } from "../../const/guestFieldMapping";
import { toast } from "react-toastify";
import useParty from "../../api/useParty";
import { Party } from "../../const/partyFieldMapping";
import { useMemo } from "react";

const ActivityMassUpdater: React.FC = () => {
  const guestQuery = useGuest();
  const partyQuery = useParty(guestQuery?.data?.[Guest.ZPartyID]);
  const [_activities, _meta, helpers] = useField("activities");

  const filteredParty = useMemo(() => {
    return (partyQuery.data || []).filter(
      (party) => party[Party.Zuuid] !== guestQuery.data?.[Guest.Zuuid]
    );
  }, [partyQuery.data, guestQuery.data]);

  const handleClick = (activities: string[]) => {
    helpers.setValue(activities);
    helpers.setTouched(true);
    toast.info("Activities Updated");
  };

  return (
    <div className="mb-2 flex flex-col space-y-2 pt-2 text-sm sm:flex-row sm:space-x-4 sm:space-y-0">
      {filteredParty.map((party) => (
        <button
          type="button"
          onClick={() => handleClick(JSON.parse(party[Party.zctActivityIds]))}
          className="btn btn-default"
        >
          COPY FROM {party[Party.NameFirst]} {party[Party.NameLast]}
        </button>
      ))}
      <button
        type="button"
        onClick={() => handleClick([])}
        className="btn btn-default"
      >
        Unselect All
      </button>
    </div>
  );
};

export default ActivityMassUpdater;
