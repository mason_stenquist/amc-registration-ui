import { useField } from "formik";
import { useMemo } from "react";
import useActivities from "../../api/useActivities";
import { Activity } from "../../const/activityFieldMapping";

const useSelectedActivities = () => {
  const [field] = useField<string[]>("activities");
  const activitiesQuery = useActivities();

  const selectedActivities = useMemo(() => {
    return (activitiesQuery?.data ?? []).filter((act) =>
      field.value.includes(act[Activity.Zuuid])
    );
  }, [activitiesQuery.data, field.value]);

  return selectedActivities;
};

export default useSelectedActivities;
