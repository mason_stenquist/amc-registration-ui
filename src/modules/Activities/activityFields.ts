import * as Yup from "yup";
import { Guest, GuestLayoutFields } from "../../const/guestFieldMapping";

export const ActivitySchema = Yup.object().shape({
  [Guest.Zuuid]: Yup.string(),
  hasGolf: Yup.boolean(),
  [Guest.GolfRentalPref]: Yup.string().when("hasGolf", {
    is: true,
    then: Yup.string().required("Required"),
  }),
  activities: Yup.array().min(1, "You must select at least one activity"),
  hasMassage: Yup.boolean(),
  [Guest.MassagePref]: Yup.string().when("hasMassage", {
    is: true,
    then: Yup.string().required("Required"),
  }),
});

export const getInitialValues = (data: GuestLayoutFields | undefined) => ({
  [Guest.Zuuid]: data?.[Guest.Zuuid] ?? "",
  activities: data?.activities ?? [],
  hasGolf: false,
  [Guest.GolfRentalPref]: data?.[Guest.GolfRentalPref] ?? "",
  hasMassage: false,
  [Guest.MassagePref]: data?.[Guest.MassagePref] ?? "",
});
