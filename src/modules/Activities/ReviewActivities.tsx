import { useMemo } from "react";
import ActivitiesModule from ".";
import useActivities from "../../api/useActivities";
import useGuest from "../../api/useGuest";
import ReviewHeader from "../../components/ReviewHeader";
import ReviewRow from "../../components/ReviewRow";
import { Activity } from "../../const/activityFieldMapping";
import { Guest } from "../../const/guestFieldMapping";

interface Props {
  id: string;
}

const ReviewActivities: React.FC<Props> = ({ id }) => {
  const { data: guest } = useGuest(id);
  const activitiesQuery = useActivities(id);
  const selectedActivityArray = guest?.[Guest.zctActivityIds] ?? [""];

  const selectedActivities = useMemo(() => {
    return (activitiesQuery?.data ?? []).filter((act) =>
      selectedActivityArray.includes(act[Activity.Zuuid])
    );
  }, [activitiesQuery.data, selectedActivityArray]);

  return (
    <div className="mb-4">
      <ReviewHeader guestId={id} module={ActivitiesModule} />

      <table className="mb-3 w-full text-xs sm:w-auto">
        <thead>
          <tr>
            <td className="hidden py-0.5 pr-2 font-bold sm:table-cell">Date</td>
            <td className="hidden py-0.5 pr-2 text-right font-bold sm:table-cell sm:w-[65px]">
              Start
            </td>
            <td className="hidden py-0.5 pr-2 text-right font-bold sm:table-cell sm:w-[65px]">
              End
            </td>
            <td className="hidden py-0.5 pr-2 font-bold sm:table-cell">
              Activity
            </td>
          </tr>
        </thead>
        <tbody className="align-text-top">
          {selectedActivities.map((act) => (
            <tr key={act[Activity.Zuuid]}>
              <td className="py-0.5 sm:pr-3">
                <p className="block sm:hidden">Date:</p>
                {act[Activity.Date]}
              </td>
              <td className="py-0.5 text-right sm:pr-3">
                <p className="block sm:hidden">Start:</p>
                {act.dayjsTimestampStart.format("h:mm A")}
              </td>
              <td className="py-0.5 text-right sm:pr-3">
                <p className="block sm:hidden">End:</p>
                {act.dayjsTimestampEnd.format("h:mm A")}
              </td>

              <td className="pt-0.5 pb-2 sm:pr-3 ">
                <p className="block sm:hidden">Activity:</p>
                {act[Activity.Activity]}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <table className="w-full">
        <tbody>
          {guest?.[Guest.MassagePref] && (
            <ReviewRow label="Masseuse Preference">
              {guest?.[Guest.MassagePref]}
            </ReviewRow>
          )}
          {guest?.[Guest.GolfRentalPref] && (
            <ReviewRow label="Golf Club Preference">
              {guest?.[Guest.GolfRentalPref]}
            </ReviewRow>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ReviewActivities;
