import { useField } from "formik";
import { useEffect, useMemo } from "react";
import Select from "../../components/Select";
import { Activity } from "../../const/activityFieldMapping";
import { Guest } from "../../const/guestFieldMapping";
import useSelectedActivities from "./useSelectedActivities";

const GolfRentalPreference = () => {
  const [field, _meta, helpers] = useField<boolean>("hasGolf");
  const { setValue } = helpers;
  const selectedActivities = useSelectedActivities();

  const showField = useMemo(() => {
    return selectedActivities.some((act) => act?.[Activity.PrefGolf]);
  }, [selectedActivities]);

  useEffect(() => {
    if (field.value !== showField) {
      setValue(showField);
    }
  }, [showField]);

  if (!showField) return null;

  return (
    <div className="py-2">
      <p className="text-lg font-bold">Golf Preferences</p>
      <Select name={Guest.GolfRentalPref} label="Golf Club Preference">
        <option></option>
        <option>I will require rental clubs</option>
        <option>I will be bringing my own clubs</option>
      </Select>
    </div>
  );
};

export default GolfRentalPreference;
