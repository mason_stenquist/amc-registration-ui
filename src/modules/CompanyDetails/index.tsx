import Input from "../../components/Input";
import { Form, Formik } from "formik";
import useGuest from "../../api/useGuest";
import { Module } from "../";
import NextPrevNavigation from "../../components/NextPrevNavigation";
import * as Yup from "yup";
import { Guest, Registration } from "../../const/guestFieldMapping";
import { useQueryClient } from "react-query";
import usePeformScript from "../../api/usePerformScript";
import { SAVE_GUEST_SCRIPT } from "../../const/const";
import { toast } from "react-toastify";
import ReviewCompanyDetails from "./ReviewCompanyDetails";
import useGoToNext from "../hooks/useGoToNext";

const CompanyDetailSchema = Yup.object().shape({
  [Guest.CompName]: Yup.string().required("Required"),
  [Guest.CompPosition]: Yup.string().required("Required"),
  [Guest.AddressSecondStreet]: Yup.string().required("Required"),
  [Guest.AddressSecondCity]: Yup.string().required("Required"),
  [Guest.AddressSecondState]: Yup.string().required("Required"),
  [Guest.AddressSecondZip]: Yup.string()
    .min(5, "Too Short")
    .max(5, "Too Long")
    .required("Required"),
});

const CompanyDetails: React.FC = () => {
  const guestQuery = useGuest();
  const queryClient = useQueryClient();
  const saveGuest = usePeformScript(SAVE_GUEST_SCRIPT);
  const next = useGoToNext(CompanyDetailsModule);

  if (guestQuery.isLoading) {
    return <div>Personal Details Loading...</div>;
  }

  if (guestQuery.isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <Formik
      initialValues={{
        [Guest.Zuuid]: guestQuery.data?.[Guest.Zuuid] ?? "",
        [Guest.CompName]: guestQuery.data?.[Guest.CompName] ?? "",
        [Guest.CompPosition]: guestQuery.data?.[Guest.CompPosition] ?? "",
        [Guest.AddressSecondStreet]:
          guestQuery.data?.[Guest.AddressSecondStreet] ?? "",
        [Guest.AddressSecondCity]:
          guestQuery.data?.[Guest.AddressSecondCity] ?? "",
        [Guest.AddressSecondState]:
          guestQuery.data?.[Guest.AddressSecondState] ?? "",
        [Guest.AddressSecondZip]:
          guestQuery.data?.[Guest.AddressSecondZip] ?? "",
      }}
      validationSchema={CompanyDetailSchema}
      onSubmit={(values) => {
        saveGuest.mutate(values, {
          onError: (e) => {
            toast.error(e?.message ?? "Error. Could not save data");
          },
          onSuccess: (data) => {
            queryClient.setQueryData(
              ["guest", guestQuery?.data?.[Guest.Zuuid]],
              { ...guestQuery.data, ...values }
            );
            next();
          },
        });
      }}
    >
      <Form className="lg:flex">
        <div className="">
          <h2 className="text-lg font-bold">{CompanyDetailsModule?.title}</h2>
          <Input name={Guest.CompName} label="Company Name" />
          <Input name={Guest.CompPosition} label="Job Title" />
          <h2 className="text-lg font-bold">Company Address</h2>
          <Input name={Guest.AddressSecondStreet} label="Company Address" />
          <div className="md:flex md:space-x-4">
            <div className="md:w-7/12">
              <Input name={Guest.AddressSecondCity} label="City" />
            </div>
            <div className="md:w-2/12">
              <Input name={Guest.AddressSecondState} label="State" />
            </div>
            <div className="md:w-3/12">
              <Input name={Guest.AddressSecondZip} label="Zip" />
            </div>
          </div>
          <input type="hidden" name={Guest.Zuuid} />
          <NextPrevNavigation
            currentModule={CompanyDetailsModule}
            mutationStatus={saveGuest.status}
          />
        </div>
      </Form>
    </Formik>
  );
};

export const CompanyDetailsModule: Module = {
  to: "/register/:guestId/company-details",
  title: "Company Details",
  element: <CompanyDetails />,
  review: (id) => <ReviewCompanyDetails id={id} />,
  schema: CompanyDetailSchema,
  showModule: (guest) =>
    guest?.[Guest.ZPartyID] === guest?.[Guest.Zuuid] &&
    guest?.[Registration.GuestType].toLowerCase() !== "staff",
};

export default CompanyDetailsModule;
