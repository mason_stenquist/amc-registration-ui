import CompanyDetailsModule from ".";
import useGuest from "../../api/useGuest";
import ReviewHeader from "../../components/ReviewHeader";
import ReviewRow from "../../components/ReviewRow";
import { Guest } from "../../const/guestFieldMapping";

interface Props {
  id: string;
}

const ReviewCompanyDetails: React.FC<Props> = ({ id }) => {
  const { data: guest } = useGuest(id);
  return (
    <div className="mb-4">
      <ReviewHeader guestId={id} module={CompanyDetailsModule} />
      <table className="w-full">
        <tbody>
          <ReviewRow label="Company Name">{guest?.[Guest.CompName]}</ReviewRow>
          <ReviewRow label="Job Title">{guest?.[Guest.CompPosition]}</ReviewRow>
          <ReviewRow label="Company Address">
            {guest?.[Guest.AddressSecondStreet]}
            <br />
            {guest?.[Guest.AddressSecondCity]},{" "}
            {guest?.[Guest.AddressSecondState]},{" "}
            {guest?.[Guest.AddressSecondZip]}
          </ReviewRow>
        </tbody>
      </table>
    </div>
  );
};

export default ReviewCompanyDetails;
