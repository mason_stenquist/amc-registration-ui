import PersonalDetailsModule from ".";
import useGuest from "../../api/useGuest";
import ReviewHeader from "../../components/ReviewHeader";
import ReviewRow from "../../components/ReviewRow";
import { Guest, Registration } from "../../const/guestFieldMapping";

interface Props {
  id: string;
}

const ReviewPersonalDetails: React.FC<Props> = ({ id }) => {
  const { data: guest } = useGuest(id);
  return (
    <div className="mb-4">
      <ReviewHeader guestId={id} module={PersonalDetailsModule} />
      <table className="w-full">
        <tbody>
          <ReviewRow label="First Name">{guest?.[Guest.NameFirst]}</ReviewRow>
          <ReviewRow label="Middle Name">{guest?.[Guest.NameMid]}</ReviewRow>
          <ReviewRow label="Last Name">{guest?.[Guest.NameLast]}</ReviewRow>
          <ReviewRow label="Email">{guest?.[Guest.Email]}</ReviewRow>
          <ReviewRow label="Mobile Phone">{guest?.[Guest.Phone]}</ReviewRow>
          <ReviewRow label="Home Address">
            {guest?.[Guest.AddressStreet]}
            <br />
            {guest?.[Guest.AddressCity]}, {guest?.[Guest.AddressState]},{" "}
            {guest?.[Guest.AddressZip]}
          </ReviewRow>
          <ReviewRow label="Special Requests">
            {guest?.[Guest.SpecialNeeds]}
          </ReviewRow>
          <ReviewRow label="Assistant Name">
            {guest?.[Registration.AssistantName]}
          </ReviewRow>
          <ReviewRow label="Assistant Email">
            {guest?.[Registration.AssistantEmail]}
          </ReviewRow>
          <ReviewRow label="Emergency Contact Info">
            {guest?.[Guest.EmergencyName]} {guest?.[Guest.EmergencyPhone]}
          </ReviewRow>
        </tbody>
      </table>
    </div>
  );
};

export default ReviewPersonalDetails;
