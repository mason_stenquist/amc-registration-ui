import Input from "../../components/Input";
import { Form, Formik } from "formik";
import useGuest from "../../api/useGuest";
import { Module } from "..";
import { Guest, Registration } from "../../const/guestFieldMapping";
import NextPrevNavigation from "../../components/NextPrevNavigation";
import { toast } from "react-toastify";
import { useQueryClient } from "react-query";
import usePeformScript from "../../api/usePerformScript";
import {
  getInitialValues,
  PersonalDetailSchema,
} from "./personalDetailFormFields";
import { SAVE_GUEST_SCRIPT } from "../../const/const";
import ReviewPersonalDetails from "./ReviewPersonalDetails";
import useGoToNext from "../hooks/useGoToNext";

const PersonalDetails = () => {
  const guestQuery = useGuest();
  const queryClient = useQueryClient();
  const saveGuest = usePeformScript(SAVE_GUEST_SCRIPT);
  const next = useGoToNext(PersonalDetailsModule);

  if (guestQuery.isLoading) {
    return <div>Personal Details Loading...</div>;
  }

  return (
    <Formik
      initialValues={getInitialValues(guestQuery?.data)}
      validationSchema={PersonalDetailSchema}
      onSubmit={async (values) => {
        saveGuest.mutate(values, {
          onError: (e) => {
            toast.error(e?.message ?? "Error. Could not save data");
          },
          onSuccess: (data) => {
            queryClient.invalidateQueries("party");
            queryClient.setQueryData(
              ["guest", guestQuery?.data?.[Guest.Zuuid]],
              { ...guestQuery.data, ...values }
            );
            next();
          },
        });
      }}
    >
      <Form className="lg:flex">
        <div className="">
          <h2 className="text-lg font-bold">{PersonalDetailsModule?.title}</h2>
          <p>
            PLEASE FILL OUT YOUR NAME AS IT APPEARS ON YOUR GOVERNMENT ISSUED ID
          </p>
          <div className="md:flex md:space-x-4">
            <div className="md:w-1/3">
              <Input
                name={Guest.NameFirst}
                label="First Name"
                autoComplete="given-name"
                autoCapitalize="on"
              />
            </div>
            <div className="md:w-1/3">
              <Input
                name={Guest.NameMid}
                label="Middle"
                optional
                autoComplete="additional-name"
                autoCapitalize="on"
              />
            </div>
            <div className="md:w-1/2">
              <Input
                name={Guest.NameLast}
                label="Last Name"
                autoComplete="family-name"
                autoCapitalize="on"
              />
            </div>
          </div>
          <Input
            name={Guest.Email}
            label="Email"
            type="email"
            autoComplete="email"
          />
          <div className="md:flex md:space-x-4">
            <div className="md:w-1/2">
              <Input
                name={Guest.Phone}
                label="Mobile Phone"
                autoComplete="tel"
              />
            </div>
            <div className="md:w-1/2">
              <Input
                type="date"
                name={Guest.Dob}
                label="Date of Birth"
                autoComplete="b-day"
              />
            </div>
          </div>
          <h2 className="text-lg font-bold">Home Address</h2>
          <Input
            name={Guest.AddressStreet}
            label="Address"
            autoComplete="street-address"
          />
          <div className="md:flex md:space-x-4">
            <div className="md:w-7/12">
              <Input
                name={Guest.AddressCity}
                label="City"
                autoComplete="address-level2"
              />
            </div>
            <div className="md:w-2/12">
              <Input
                name={Guest.AddressState}
                label="State"
                autoComplete="address-level1"
              />
            </div>
            <div className="md:w-3/12">
              <Input
                name={Guest.AddressZip}
                label="Zip"
                autoComplete="postal-code"
              />
            </div>
          </div>
          <p className="pb-4 text-sm text-gray-700">
            In the interest of everyone’s safety, we will be utilizing this
            address to send you an at-home covid test prior to the trip
          </p>
          <h2 className="text-lg font-bold">Additional Information</h2>
          <Input
            name={Guest.SpecialNeeds}
            label="Special Requests"
            optional
            placeholder="Dietary or Otherwise"
          />
          <div className="w-full md:flex md:space-x-4">
            <div className="md:w-1/2">
              <Input
                name={Registration.AssistantName}
                label="Assistant Name"
                optional
                autoComplete="nope"
              />
            </div>
            <div className="md:w-1/2">
              <Input
                name={Registration.AssistantEmail}
                label="Assistant Email"
                optional
                autoComplete="nope"
              />
            </div>
          </div>
          <h2 className="text-lg font-bold">Emergency Contact Info</h2>
          <p className="mb-2 text-sm text-gray-600">
            Someone not traveling with you
          </p>
          <div className="w-full md:flex md:space-x-4">
            <div className="md:w-1/2">
              <Input
                name={Guest.EmergencyName}
                label="Name"
                autoComplete="nope"
              />
            </div>
            <div className="md:w-1/2">
              <Input
                name={Guest.EmergencyPhone}
                label="Mobile Phone Number"
                autoComplete="nope"
              />
            </div>
          </div>
          <input type="hidden" name={Guest.Zuuid} />
          <NextPrevNavigation
            currentModule={PersonalDetailsModule}
            mutationStatus={saveGuest.status}
          />
        </div>
      </Form>
    </Formik>
  );
};

const PersonalDetailsModule: Module = {
  to: "/register/:guestId/personal-details",
  title: "Personal Details",
  element: <PersonalDetails />,
  review: (id) => <ReviewPersonalDetails id={id} />,
  schema: PersonalDetailSchema,
  showModule: (guest) => true,
};

export default PersonalDetailsModule;
