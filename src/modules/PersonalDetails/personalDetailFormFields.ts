import * as Yup from "yup";
import {
  Guest,
  GuestLayoutFields,
  Registration,
} from "../../const/guestFieldMapping";

export const PersonalDetailSchema = Yup.object().shape({
  [Guest.NameFirst]: Yup.string().required("Required"),
  [Guest.NameLast]: Yup.string().required("Required"),
  [Guest.Email]: Yup.string().email("Invalid email").required("Required"),
  [Guest.Phone]: Yup.string().required("Required"),
  [Guest.Dob]: Yup.date().required("Required").label("DOB"),
  [Guest.AddressStreet]: Yup.string().required("Required"),
  [Guest.AddressCity]: Yup.string().required("Required"),
  [Guest.AddressState]: Yup.string().required("Required"),
  [Guest.AddressZip]: Yup.string()
    .min(5, "Too Short")
    .max(5, "Too Long")
    .required("Required"),
  [Registration.AssistantEmail]: Yup.string().email("Invalid email"),
  [Guest.EmergencyName]: Yup.string().required("Required"),
  [Guest.EmergencyPhone]: Yup.string().required("Required"),
});

export const getInitialValues = (data: GuestLayoutFields | undefined) => ({
  [Guest.Zuuid]: data?.[Guest.Zuuid] ?? "",
  [Guest.NameFirst]: data?.[Guest.NameFirst] ?? "",
  [Guest.NameMid]: data?.[Guest.NameMid] ?? "",
  [Guest.NameLast]: data?.[Guest.NameLast] ?? "",
  [Guest.Email]: data?.[Guest.Email] ?? "",
  [Guest.Phone]: data?.[Guest.Phone] ?? "",
  [Guest.Dob]: data?.[Guest.Dob] ?? "",
  [Guest.AddressStreet]: data?.[Guest.AddressStreet] ?? "",
  [Guest.AddressCity]: data?.[Guest.AddressCity] ?? "",
  [Guest.AddressState]: data?.[Guest.AddressState] ?? "",
  [Guest.AddressZip]: data?.[Guest.AddressZip] ?? "",
  [Guest.SpecialNeeds]: data?.[Guest.SpecialNeeds] ?? "",
  [Registration.AssistantName]: data?.[Registration.AssistantName] ?? "",
  [Registration.AssistantEmail]: data?.[Registration.AssistantEmail] ?? "",
  [Guest.EmergencyName]: data?.[Guest.EmergencyName] ?? "",
  [Guest.EmergencyPhone]: data?.[Guest.EmergencyPhone] ?? "",
});
