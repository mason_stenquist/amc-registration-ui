import Input from "../../components/Input";
import { Registration } from "../../const/guestFieldMapping";
import Select from "../../components/Select";
import { useField } from "formik";
import { TextBool } from "../../const/const";
import TextArea from "../../components/TextArea";
import classNames from "classnames";

const LodgingInformation: React.FC = () => {
  const [{ value: needHotel }] = useField(Registration.NeedHotel);
  const specialRequestClasses = classNames(
    "transition-all duration-300 overflow-hidden",
    {
      "max-h-[0]": needHotel === TextBool.No,
    },
    {
      "max-h-[300px]": needHotel === TextBool.Yes,
    }
  );
  return (
    <>
      <h2 className="text-lg font-bold">Lodging Information</h2>
      <div className="md:flex md:space-x-4">
        <div className="md:w-1/3">
          <Select
            name={Registration.NeedHotel}
            label="Will you require lodging?"
          >
            <option></option>
            <option>{TextBool.Yes}</option>
            <option>{TextBool.No}</option>
          </Select>
        </div>
        <div className="md:w-2/3">
          {needHotel === TextBool.No && (
            <Input
              name={Registration.NeedHotelReason}
              label="If No, please explain"
            />
          )}
          {needHotel === TextBool.Yes && (
            <Select name={Registration.HousingBedType} label="Room Type">
              <option></option>
              <option>King</option>
              <option>Two Queen Beds</option>
            </Select>
          )}
        </div>
      </div>
      <div className={specialRequestClasses}>
        <TextArea
          name={Registration.SpecialNeedsHousing}
          label="Please detail any special lodging requests"
          optional
          rows={3}
        />
      </div>
    </>
  );
};

export default LodgingInformation;
