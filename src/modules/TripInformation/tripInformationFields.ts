import * as Yup from "yup";
import { TextBool } from "../../const/const";
import {
  Guest,
  GuestLayoutFields,
  Registration,
} from "../../const/guestFieldMapping";
import { NeedAirReason } from "./FlightInformation";

export const TripInformationSchema = Yup.object().shape({
  [Guest.Zuuid]: Yup.string(),
  [Guest.ZPartyID]: Yup.string(),
  [Registration.GuestType]: Yup.string(),
  [Registration.NeedAir]: Yup.string().required("Required"),
  [Registration.NeedAirReason]: Yup.string().when(Registration.NeedAir, {
    is: TextBool.No,
    then: Yup.string().required("Required"),
  }),
  [Registration.DateAirArrival]: Yup.string().required("Required"),
  [Registration.AirportCodeArrival]: Yup.string()
    .min(3, "Use 3 digit code")
    .max(3, "Use 3 digit code")
    .required("Required"),
  [Registration.ArrivalTailNumber]: Yup.string().when(
    Registration.NeedAirReason,
    {
      is: NeedAirReason.Private,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.DateAirDeparture]: Yup.string().required("Required"),
  [Registration.AirportCodeDeparture]: Yup.string()
    .min(3, "Use 3 digit code")
    .max(3, "Use 3 digit code")
    .required("Required"),
  [Registration.DepartureTailNumber]: Yup.string().when(
    Registration.NeedAirReason,
    {
      is: NeedAirReason.Private,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.NeedHotel]: Yup.string().when(
    [Guest.Zuuid, Guest.ZPartyID, Registration.GuestType],
    {
      is: (guestId: string, partyId: string, guestType: string) =>
        guestId === partyId && (guestType || "").toLowerCase() !== "staff",
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.NeedHotelReason]: Yup.string().when(Registration.NeedHotel, {
    is: TextBool.No,
    then: Yup.string().required("Required"),
  }),
  [Registration.HousingBedType]: Yup.string().when(Registration.NeedHotel, {
    is: TextBool.Yes,
    then: Yup.string().required("Required"),
  }),
  [Registration.NeedGround]: Yup.string().required("Required"),
  [Registration.NeedGroundReason]: Yup.string().when(Registration.NeedGround, {
    is: TextBool.No,
    then: Yup.string().required("Required"),
  }),
  [Registration.ArrivalAirline]: Yup.string().when(
    [Registration.NeedAir, Registration.NeedAirReason],
    {
      is: (needAir: TextBool, needAirReason: NeedAirReason) =>
        needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.ArrivalFlightNo]: Yup.string().when(
    [Registration.NeedAir, Registration.NeedAirReason],
    {
      is: (needAir: TextBool, needAirReason: NeedAirReason) =>
        needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.DepatureAirline]: Yup.string().when(
    [Registration.NeedAir, Registration.NeedAirReason],
    {
      is: (needAir: TextBool, needAirReason: NeedAirReason) =>
        needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.DepatureFlightNo]: Yup.string().when(
    [Registration.NeedAir, Registration.NeedAirReason],
    {
      is: (needAir: TextBool, needAirReason: NeedAirReason) =>
        needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking,
      then: Yup.string().required("Required"),
    }
  ),
});

export const getInitialValues = (data: GuestLayoutFields | undefined) => ({
  [Guest.Zuuid]: data?.[Guest.Zuuid] ?? "",
  [Guest.ZPartyID]: data?.[Guest.ZPartyID] ?? "",
  [Registration.NeedAir]: data?.[Registration.NeedAir] ?? "",
  [Registration.NeedAirReason]: data?.[Registration.NeedAirReason] ?? "",
  [Registration.DateAirArrival]: data?.[Registration.DateAirArrival] ?? "",
  [Registration.AirportCodeArrival]:
    data?.[Registration.AirportCodeArrival] ?? "",
  [Registration.ArrivalTailNumber]:
    data?.[Registration.ArrivalTailNumber] ?? "",
  [Registration.DateAirDeparture]: data?.[Registration.DateAirDeparture] ?? "",
  [Registration.AirportCodeDeparture]:
    data?.[Registration.AirportCodeDeparture] ?? "",
  [Registration.DepartureTailNumber]:
    data?.[Registration.DepartureTailNumber] ?? "",
  [Registration.ArrivalAirline]: data?.[Registration.ArrivalAirline] ?? "",
  [Registration.ArrivalFlightNo]: data?.[Registration.ArrivalFlightNo] ?? "",
  [Registration.DepatureAirline]: data?.[Registration.DepatureAirline] ?? "",
  [Registration.DepatureFlightNo]: data?.[Registration.DepatureFlightNo] ?? "",
  [Guest.FreqFlyerCarrier]: data?.[Guest.FreqFlyerCarrier] ?? "",
  [Guest.FreqFlyerNo]: data?.[Guest.FreqFlyerNo] ?? "",
  [Registration.NeedGround]: data?.[Registration.NeedGround] ?? "",
  [Registration.NeedGroundReason]: data?.[Registration.NeedGroundReason] ?? "",
  [Registration.NeedHotel]: data?.[Registration.NeedHotel] ?? "",
  [Registration.NeedHotelReason]: data?.[Registration.NeedHotelReason] ?? "",
  [Registration.SpecialNeedsHousing]:
    data?.[Registration.SpecialNeedsHousing] ?? "",
  [Registration.HousingBedType]: data?.[Registration.HousingBedType] ?? "",
  [Registration.GuestType]: data?.[Registration.GuestType] ?? "",
});
