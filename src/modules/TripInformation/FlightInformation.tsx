import Input from "../../components/Input";
import { Guest, Registration } from "../../const/guestFieldMapping";
import Select from "../../components/Select";
import { useField } from "formik";
import { EVENT_AIR_CODE, TextBool } from "../../const/const";
import TextArea from "../../components/TextArea";
import DataList from "../../components/DataList";
import { airportOptions } from "../../const/airportOptions";
import HideIfStaff from "../../components/HideIfStaff";
import useGuest from "../../api/useGuest";
import { useEffect, useMemo } from "react";
import { airlineOptions } from "../../const/airlineOptions";
import dayjs from "../../utils/dayJS";
import { generateDateArrayBetweenDates } from "../../utils/dateUtils";

export const enum NeedAirReason {
  Private = "Private plane",
  SelfBooking = "Booking flight on own",
}

const FlightInformation: React.FC = () => {
  const guestQuery = useGuest();
  const [{ value: needAir }, , needAirHelpers] = useField(Registration.NeedAir);
  const [{ value: needAirReason }, , needAirReasonHelpers] = useField(
    Registration.NeedAirReason
  );
  const [{ value: NeedGround }] = useField(Registration.NeedGround);

  const arrivalDateOptions: React.ReactElement<HTMLOptionElement>[] =
    useMemo(() => {
      const array = generateDateArrayBetweenDates(
        guestQuery.data?.dayjsArrivalStart,
        guestQuery.data?.dayjsArrivalEnd
      );
      return array.map((date) => (
        <option
          key={date.format("YYYY-MM-DD")}
          value={date.format("YYYY-MM-DD")}
        >
          {date.format("M/D/YYYY - dddd")}
        </option>
      ));
    }, [guestQuery.data]);

  const depatureDateOptions: React.ReactElement<HTMLOptionElement>[] =
    useMemo(() => {
      const array = generateDateArrayBetweenDates(
        guestQuery.data?.dayjsDepatureStart,
        guestQuery.data?.dayjsDepatureEnd
      );
      return array.map((date) => (
        <option
          key={date.format("YYYY-MM-DD")}
          value={date.format("YYYY-MM-DD")}
        >
          {date.format("M/D/YYYY - dddd")}
        </option>
      ));
    }, [guestQuery.data]);

  useEffect(() => {
    if (guestQuery.data?.[Registration.GuestType].toLowerCase() === "staff") {
      needAirHelpers.setValue(TextBool.No);
      needAirReasonHelpers.setValue(NeedAirReason.SelfBooking);
    }
  }, [guestQuery.data]);
  return (
    <>
      <h2 className="text-lg font-bold">Travel Information</h2>

      <div className="md:flex md:space-x-4">
        <div className="md:w-1/2">
          <Select
            disabled={
              guestQuery.data?.[Registration.GuestType].toLowerCase() ===
              "staff"
            }
            name={Registration.NeedAir}
            label="Will you require travel arrangements?"
          >
            <option></option>
            <option>{TextBool.Yes}</option>
            <option>{TextBool.No}</option>
          </Select>
        </div>
        <div className="md:w-1/2">
          {needAir === TextBool.No && (
            <Select
              disabled={
                guestQuery.data?.[Registration.GuestType].toLowerCase() ===
                "staff"
              }
              name={Registration.NeedAirReason}
              label="Please select why"
            >
              <option></option>
              <option>{NeedAirReason.Private}</option>
              <option>{NeedAirReason.SelfBooking}</option>
            </Select>
          )}
        </div>
      </div>

      <p className="mb-2 font-bold">Arrival</p>
      <div className="md:flex md:space-x-4">
        <div className="md:w-1/2">
          <Select name={Registration.DateAirArrival} label="Arrival Date">
            <option></option>
            {arrivalDateOptions}
          </Select>
        </div>
        <div className="md:w-1/2">
          <p className="pb-1 text-sm text-gray-600">Airport</p>
          <div className="flex items-end space-x-4">
            <DataList
              name={Registration.AirportCodeArrival}
              placeholder="Airport"
              autoComplete="nope"
            >
              {airportOptions.map(({ value, label }) => (
                <option value={value} key={value}>
                  {label}
                </option>
              ))}
            </DataList>
            <p className="block flex-grow pb-5 text-center text-sm">to</p>
            <Input
              name="__arrivalFlightDestAirport"
              placeholder={EVENT_AIR_CODE}
              disabled
            />
            {needAir === TextBool.No &&
              needAirReason === NeedAirReason.Private && (
                <Input
                  name={Registration.ArrivalTailNumber}
                  placeholder="Tail Number"
                />
              )}
          </div>
        </div>
      </div>
      {needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking && (
        <div className="md:flex md:space-x-4">
          <div className="md:w-1/2">
            <DataList
              label="Arrival Airline Code"
              name={Registration.ArrivalAirline}
            >
              {airlineOptions.map(({ value, label }) => (
                <option key={value} value={value}>
                  {label}
                </option>
              ))}
            </DataList>
          </div>
          <div className="md:w-1/2">
            <Input
              label="Arrival Flight Number"
              name={Registration.ArrivalFlightNo}
            />
          </div>
        </div>
      )}

      <p className="mb-2 font-bold">Departure</p>
      <div className="md:flex md:space-x-4">
        <div className="md:w-1/2">
          <Select name={Registration.DateAirDeparture} label="Departure Date">
            <option></option>
            {depatureDateOptions}
          </Select>
        </div>
        <div className="md:w-1/2">
          <p className="pb-1 text-sm text-gray-600">Airport</p>
          <div className="flex items-end space-x-4 ">
            <Input
              name="__departureFlightOriginAirport"
              placeholder={EVENT_AIR_CODE}
              disabled
            />
            <p className="block flex-grow pb-5 text-center text-sm">to</p>
            <DataList
              name={Registration.AirportCodeDeparture}
              placeholder="Airport"
              autoComplete="nope"
            >
              {airportOptions.map(({ value, label }) => (
                <option value={value} key={value}>
                  {label}
                </option>
              ))}
            </DataList>
            {needAir === TextBool.No &&
              needAirReason === NeedAirReason.Private && (
                <Input
                  name={Registration.DepartureTailNumber}
                  placeholder="Tail Number"
                />
              )}
          </div>
        </div>
      </div>

      {needAir === TextBool.No && needAirReason === NeedAirReason.SelfBooking && (
        <div className="md:flex md:space-x-4">
          <div className="md:w-1/2">
            <DataList
              label="Departure Airline Code"
              name={Registration.DepatureAirline}
            >
              {airlineOptions.map(({ value, label }) => (
                <option key={value} value={value}>
                  {label}
                </option>
              ))}
            </DataList>
          </div>
          <div className="md:w-1/2">
            <Input
              label="Depature Flight Number"
              name={Registration.DepatureFlightNo}
            />
          </div>
        </div>
      )}

      <HideIfStaff guest={guestQuery.data}>
        <p className="mb-2 font-bold">Frequent Flyer Information</p>
        <div className="md:flex md:space-x-4">
          <div className="md:w-1/2">
            <Input
              name={Guest.FreqFlyerCarrier}
              label="Frequent Flyer Carrier"
              optional
            />
          </div>
          <div className="md:w-1/2">
            <Input
              name={Guest.FreqFlyerNo}
              label="Frequent Flyer Number"
              optional
            />
          </div>
        </div>
      </HideIfStaff>

      <HideIfStaff guest={guestQuery.data}>
        <TextArea
          name={Registration.SpecialNeedsTravel}
          label="Please detail any special travel requests"
          optional
          rows={3}
        />
      </HideIfStaff>

      <Select
        name={Registration.NeedGround}
        label={`Upon arrival in ${EVENT_AIR_CODE}, will you require ground transportation?`}
      >
        <option></option>
        <option>{TextBool.Yes}</option>
        <option>{TextBool.No}</option>
      </Select>

      {NeedGround === TextBool.No && (
        <TextArea
          name={Registration.NeedGroundReason}
          label="Please explain why you will not require ground transportation"
          rows={3}
        />
      )}
    </>
  );
};

export default FlightInformation;
