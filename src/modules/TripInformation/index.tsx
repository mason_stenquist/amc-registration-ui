import { Form, Formik } from "formik";
import useGuest from "../../api/useGuest";
import { Module } from "..";
import NextPrevNavigation from "../../components/NextPrevNavigation";
import { Guest } from "../../const/guestFieldMapping";
import { useQueryClient } from "react-query";
import usePeformScript from "../../api/usePerformScript";
import { SAVE_GUEST_SCRIPT } from "../../const/const";
import { toast } from "react-toastify";
import LodgingInformation from "./LodgingInformation";
import FlightInformation from "./FlightInformation";
import {
  getInitialValues,
  TripInformationSchema,
} from "./tripInformationFields";
import ShowOnlyOnPrimary from "../../components/HideIfNotPrimary";
import ReviewTripInformation from "./ReviewTripInformation";
import useGoToNext from "../hooks/useGoToNext";
import HideIfStaff from "../../components/HideIfStaff";

const TripInformation: React.FC = () => {
  const guestQuery = useGuest();
  const queryClient = useQueryClient();
  const saveGuest = usePeformScript(SAVE_GUEST_SCRIPT);
  const next = useGoToNext(TripInformationModule);

  if (guestQuery.isLoading) {
    return <div>Trip Information Loading...</div>;
  }

  if (guestQuery.isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <Formik
      initialValues={getInitialValues(guestQuery.data)}
      validationSchema={TripInformationSchema}
      onSubmit={(values) => {
        saveGuest.mutate(values, {
          onError: (e) => {
            toast.error(e?.message ?? "Error. Could not save data");
          },
          onSuccess: (data) => {
            queryClient.setQueryData(
              ["guest", guestQuery?.data?.[Guest.Zuuid]],
              { ...guestQuery.data, ...values }
            );
            next();
          },
        });
      }}
    >
      {({ errors, touched }) => {
        return (
          <Form className="lg:flex">
            <div className="w-full">
              <FlightInformation />
              <ShowOnlyOnPrimary guest={guestQuery.data}>
                <HideIfStaff guest={guestQuery.data}>
                  <LodgingInformation />
                </HideIfStaff>
              </ShowOnlyOnPrimary>

              <input type="hidden" name={Guest.Zuuid} />
              <HideIfStaff guest={guestQuery.data}>
                <p>
                  PLEASE NOTE: In order to start your travel arrangements we
                  will need to{" "}
                  <strong>
                    receive a signed copy of our Health and Safety Waiver
                  </strong>{" "}
                  for each person in your party. Look for the link in your
                  registration confirmation.
                </p>
              </HideIfStaff>
              <NextPrevNavigation
                currentModule={TripInformationModule}
                mutationStatus={saveGuest.status}
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export const TripInformationModule: Module = {
  to: "/register/:guestId/trip-information",
  title: "Trip Information",
  element: <TripInformation />,
  review: (id) => <ReviewTripInformation id={id} />,
  schema: TripInformationSchema,
  showModule: (guest) => true,
};

export default TripInformationModule;
