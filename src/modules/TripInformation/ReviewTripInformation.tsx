import TripInformationModule from ".";
import useGuest from "../../api/useGuest";
import ShowOnlyOnPrimary from "../../components/HideIfNotPrimary";
import ReviewHeader from "../../components/ReviewHeader";
import ReviewRow from "../../components/ReviewRow";
import { EVENT_AIR_CODE } from "../../const/const";
import { Guest, Registration } from "../../const/guestFieldMapping";

interface Props {
  id: string;
}

const ReviewTripInformation: React.FC<Props> = ({ id }) => {
  const { data: guest } = useGuest(id);
  return (
    <div className="mb-4">
      <ReviewHeader guestId={id} module={TripInformationModule} />
      <table className="w-full">
        <ReviewRow label="Travel Arrangements?">
          {guest?.[Registration.NeedAir]} {guest?.[Registration.NeedAirReason]}
        </ReviewRow>
        <ReviewRow label="Arrival Date">
          {guest?.[Registration.DateAirArrival]}
        </ReviewRow>
        <ReviewRow label="Arrival Flight">
          {guest?.[Registration.AirportCodeArrival].toUpperCase()} to{" "}
          {EVENT_AIR_CODE} {guest?.[Registration.ArrivalTailNumber]}
        </ReviewRow>
        <ReviewRow label="Departure Date">
          {guest?.[Registration.DateAirDeparture]}
        </ReviewRow>
        <ReviewRow label="Departure Flight">
          {EVENT_AIR_CODE} to{" "}
          {guest?.[Registration.AirportCodeDeparture].toUpperCase()}{" "}
          {guest?.[Registration.DepartureTailNumber]}
        </ReviewRow>
        <ReviewRow label="Frequent Flyer Info">
          {guest?.[Guest.FreqFlyerCarrier] &&
            guest?.[Guest.FreqFlyerCarrier] + "  " + guest?.[Guest.FreqFlyerNo]}
        </ReviewRow>
        <ReviewRow label="Special Travel Requests">
          {guest?.[Registration.SpecialNeedsTravel]}
        </ReviewRow>
        <ReviewRow label="Ground Transportation?">
          {guest?.[Registration.NeedGround]}{" "}
          {guest?.[Registration.NeedGroundReason]}
        </ReviewRow>
        <ShowOnlyOnPrimary guest={guest}>
          <ReviewRow label="Require Housing?">
            {guest?.[Registration.NeedHotel]}{" "}
            {guest?.[Registration.NeedHotelReason]}
          </ReviewRow>
          <ReviewRow label="Lodging Requests">
            {guest?.[Registration.SpecialNeedsHousing]}
          </ReviewRow>
        </ShowOnlyOnPrimary>
      </table>
    </div>
  );
};

export default ReviewTripInformation;
