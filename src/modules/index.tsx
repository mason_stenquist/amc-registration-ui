import CompanyDetailsModule from "./CompanyDetails";
import CovidModule from "./CovidInformation";
import PersonalDetailsModule from "./PersonalDetails";
import TripInformationModule from "./TripInformation";
import * as Yup from "yup";
import { Guest, GuestLayoutFields } from "../const/guestFieldMapping";
import ActivitiesModule from "./Activities";

export interface Module {
  to: string;
  title: string;
  element: JSX.Element;
  review: (id: string) => JSX.Element;
  schema: Yup.ObjectSchema<any>;
  showModule: (guest?: GuestLayoutFields) => boolean;
}

export const entireRegistrationFlow: Module[] = [
  PersonalDetailsModule,
  CompanyDetailsModule,
  TripInformationModule,
  ActivitiesModule,
  CovidModule,
];

export const getGuestFlow = (guest?: GuestLayoutFields) =>
  entireRegistrationFlow.filter((module) => module.showModule(guest));

export const getCurrentModuleByRouteId = (routeId: string) => {
  return entireRegistrationFlow.find((module) => module.to === routeId);
};

export const getNextModule = (
  guest: GuestLayoutFields | undefined,
  currentModule: Module
) => {
  const flow = getGuestFlow(guest);
  const currentIndex = flow.findIndex((flow) => flow.to === currentModule.to);
  return currentIndex === flow.length - 1 ? null : flow[currentIndex + 1];
};

export const getPrevModule = (
  guest: GuestLayoutFields | undefined,
  currentModule: Module
) => {
  const flow = getGuestFlow(guest);
  const currentIndex = flow.findIndex((flow) => flow.to === currentModule.to);
  return currentIndex === 0 ? null : flow[currentIndex - 1];
};
