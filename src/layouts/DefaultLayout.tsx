import BackgroundImage from "../assets/background.png";
import FooterLogos from "../assets/footer-logos.png";
import useGuest from "../api/useGuest";
import { ReactNode } from "react";
import { generatePath, NavLink } from "react-router-dom";
import { Guest } from "../const/guestFieldMapping";
import useParty from "../api/useParty";
import { Party } from "../const/partyFieldMapping";
interface Props {
  subtitle?: string;
  hideNav?: boolean;
  children: ReactNode;
}

const DefaultLayout: React.FC<Props> = ({ subtitle, children }) => {
  const guestQuery = useGuest();
  const partyQuery = useParty(guestQuery?.data?.[Guest.ZPartyID]);

  return (
    <div
      className="min-h-[100vh] bg-fixed bg-center"
      style={{ backgroundImage: `url(${BackgroundImage})` }}
    >
      <div className="mx-auto  max-w-screen-lg p-2 md:p-8">
        <div className="border-l-8 border-brand bg-white p-4 print:border-l-0 md:p-8">
          <div className=" pb-4 md:flex">
            <div className="text-center md:text-left">
              <h1 className="text-xl font-thin print:hidden md:text-3xl">
                Welcome {guestQuery.data?.Name_First}{" "}
                {guestQuery.data?.Name_Last}
              </h1>
              <p className="text-sm font-thin print:hidden md:text-base">
                {subtitle}
              </p>
              <nav className="mt-6 flex flex-col items-start text-sm print:hidden sm:flex-row sm:space-x-6">
                {guestQuery.data?.[Guest.ZPartyID] && (
                  <NavLink
                    to={generatePath("/overview/:partyId", {
                      partyId: guestQuery.data?.[Guest.ZPartyID] ?? "",
                    })}
                    className={({ isActive }) => (isActive ? "underline" : "")}
                  >
                    Dashboard
                  </NavLink>
                )}

                {partyQuery.data?.map((party) => (
                  <NavLink
                    key={party[Party.Zuuid]}
                    to={generatePath("/register/:guestId", {
                      guestId: party[Party.Zuuid] ?? "",
                    })}
                    className={({ isActive }) => (isActive ? "underline" : "")}
                  >
                    {`${party[Party.NameFirst]} ${party[Party.NameLast]}`}
                  </NavLink>
                ))}
              </nav>
            </div>
          </div>
          {children}
        </div>
        <div className="py-8 text-center">
          <img src={FooterLogos} className="pl-1 md:pl-6" />
        </div>
      </div>
    </div>
  );
};

export default DefaultLayout;
