import classNames from "classnames";
import { useQueryClient } from "react-query";
import { Updater } from "react-query/types/core/utils";
import { generatePath, Link } from "react-router-dom";
import { toast } from "react-toastify";
import usePeformScript from "../../api/usePerformScript";
import ModuleStatusLinks from "../../components/ModuleStatusLinks";
import { DELETE_GUEST_SCRIPT } from "../../const/const";
import { PartyLayoutFields, Party } from "../../const/partyFieldMapping";
import PersonalDetailsModule from "../../modules/PersonalDetails";

interface Props {
  guest: PartyLayoutFields;
}

const OverviewCard: React.FC<Props> = ({ guest }) => {
  const deleteGuest = usePeformScript(DELETE_GUEST_SCRIPT);
  const queryClient = useQueryClient();
  const fullName = `${guest[Party.NameFirst]} ${guest[Party.NameLast]}`;

  const handleDelete = async (e: any) => {
    e.preventDefault();
    if (confirm("Are you sure you want to delete this guest?")) {
      deleteGuest.mutate(
        { [Party.Zuuid]: guest[Party.Zuuid] },
        {
          onSuccess: (data) => {
            queryClient.setQueryData<PartyLayoutFields[]>("party", (old) =>
              (old || []).filter(
                (p) => p?.[Party.Zuuid] !== guest?.[Party.Zuuid]
              )
            );
            toast.info("Guest has been deleted");
          },
        }
      );
    }
  };
  return (
    <Link
      to={generatePath(PersonalDetailsModule.to, {
        guestId: guest[Party.Zuuid],
      })}
      className={classNames(
        "border bg-white p-4 text-left transition-all duration-300 hover:shadow-lg",
        { "bg-green-50": guest.isComplete }
      )}
    >
      <div className="mb-2 justify-between md:flex">
        <p className="text-lg font-bold">{fullName}</p>
        {guest[Party.Zuuid] !== guest[Party.ZPartyID] && (
          <button
            onClick={handleDelete}
            className="btn btn-default hover:text-rose-600 hover:underline"
            disabled={deleteGuest.isLoading}
          >
            {deleteGuest.isLoading ? "Deleting Guest..." : "Remove Guest"}
          </button>
        )}
      </div>
      <ModuleStatusLinks guestId={guest[Party.Zuuid]} />
    </Link>
  );
};

export default OverviewCard;
