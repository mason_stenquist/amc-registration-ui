import classNames from "classnames";
import { useMemo } from "react";
import { useQueryClient } from "react-query";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import useGuest from "../../api/useGuest";
import useParty from "../../api/useParty";
import usePeformScript from "../../api/usePerformScript";
import { CREATE_GUEST_SCRIPT } from "../../const/const";
import { Guest, Registration } from "../../const/guestFieldMapping";
import { Party } from "../../const/partyFieldMapping";
import PersonalDetailsModule from "../../modules/PersonalDetails";
import Register from "../register/Register";
import OverviewCard from "./OverviewCard";

const Overview = () => {
  const { partyId } = useParams();
  const queryClient = useQueryClient();
  const partyQuery = useParty();
  const guestQuery = useGuest();
  const createGuest = usePeformScript<{ guestId: string }>(CREATE_GUEST_SCRIPT);
  const navigate = useNavigate();

  const canAddGuests = useMemo(
    () => (partyQuery?.data || []).length < 2,
    [partyQuery.data]
  );

  const addGuest = () => {
    if (!canAddGuests) {
      toast.warn("Sorry, you are only allowed to add one additional guest");
      return;
    }
    createGuest.mutate(
      {
        [Guest.ZPartyID]: partyId,
        [Registration.GuestType]: guestQuery.data?.[Registration.GuestType],
      },
      {
        onError: (err) => {
          console.log(err);
          toast.error("Error: Could not create guest");
        },
        onSuccess: (data) => {
          queryClient.invalidateQueries("party");
          navigate(generatePath(PersonalDetailsModule.to, data));
        },
      }
    );
  };

  const isComplete = useMemo(() => {
    return (
      (partyQuery.data || []).length ===
      (partyQuery.data || []).filter((p) => p.isComplete).length
    );
  }, [partyQuery.data]);

  const reviewApplication = () => {
    if (!isComplete) {
      toast.warn("Please make sure all data is completed first");
      return;
    }
    navigate(`/review/${partyId}`);
  };

  if (partyQuery.isLoading) {
    return <div>Overview Loading...</div>;
  }

  return (
    <>
      <div className="grid w-full grid-cols-1 gap-4 pb-8">
        {partyQuery.data?.map((guest) => (
          <OverviewCard key={guest[Party.Zuuid]} guest={guest} />
        ))}
      </div>
      <div className="flex flex-col justify-end space-y-6 md:flex-row md:space-y-0 md:space-x-6">
        <button
          onClick={addGuest}
          className={classNames("btn btn-default", {
            "opacity-30": !canAddGuests,
          })}
        >
          + Add Additional Guest
        </button>

        <button
          onClick={reviewApplication}
          className={classNames("btn btn-brand", {
            "opacity-30": !isComplete,
          })}
        >
          Review Application
        </button>
      </div>
    </>
  );
};

export default Overview;
