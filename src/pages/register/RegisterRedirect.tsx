import { generatePath, Navigate, useParams } from "react-router-dom";
import PersonalDetailsModule from "../../modules/PersonalDetails";

const RegisterRedirect = () => {
  const params = useParams();

  return (
    <Navigate replace to={generatePath(PersonalDetailsModule.to, params)} />
  );
};

export default RegisterRedirect;
