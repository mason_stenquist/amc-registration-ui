import { Outlet, useParams } from "react-router-dom";
import ModuleStatusLinks from "../../components/ModuleStatusLinks";

const Register = () => {
  const { guestId } = useParams();
  return (
    <>
      <div className="-mx-4 space-x-6 border-t px-4 md:-mx-8 md:px-8 lg:flex">
        <div className="flex-grow pt-6 lg:w-[600px]">
          <Outlet key={guestId} />
        </div>
        <div className="hidden border-l pl-3 pt-4 lg:block lg:w-[200px]">
          <ModuleStatusLinks guestId={guestId} />
        </div>
      </div>
    </>
  );
};

export default Register;
