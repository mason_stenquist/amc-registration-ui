const Complete = () => {
  return (
    <div className="-mx-8 border-t p-2 text-center print:border-t-0 md:p-8">
      <h1 className="font-bold print:hidden md:text-3xl">
        Thank you for submitting your registration
      </h1>
      <p className=" font-semibold">
        You will receive an email confirmation with your registration details
        shortly
      </p>

      <p className="mt-8">
        Should you need assistance with the registration site, please contact:
      </p>
      <p className="my-2 leading-tight">
        <strong>Lauren Bekesh</strong>
        <br />
        212-324-4602
        <br />
        <a
          className="text-underline text-blue-700"
          href="mailto:lauren.bekesh@amcnetworks.com"
        >
          lauren.bekesh@amcnetworks.com
        </a>
      </p>

      <p className="mt-8">All air travel inquiries should be directed to:</p>
      <p className="my-2 leading-tight">
        <strong>Karen Taylor - World Travel</strong>
        <br />
        Monday - Friday, 9:00 AM to 5:30 PM EST
        <br />
        <a
          className="text-underline text-blue-700"
          href="mailto:ktaylor@worldtravelinc.com"
        >
          ktaylor@worldtravelinc.com
        </a>
      </p>

      <p className="mt-8">Questions regarding the retreat, please contact:</p>
      <p className="my-2 leading-tight">
        <strong>Stephanie Pennachio</strong>
        <br />
        845-337-0132
        <br />
        <a
          className="text-underline text-blue-700"
          href="mailto:Stephanie.Pennachio@amcnetworks.com"
        >
          Stephanie.Pennachio@amcnetworks.com
        </a>
      </p>
    </div>
  );
};

export default Complete;
