import classNames from "classnames";
import { useField } from "formik";
import { useMemo } from "react";
import { Registration } from "../../const/guestFieldMapping";

interface Props {
  children: JSX.Element;
}

const CompletedByForm: React.FC<Props> = ({ children }) => {
  const [field] = useField(Registration.CompletedOther);

  const completedByClasses = classNames(
    "overflow-hidden transition-all duration-300",
    {
      "max-h-[0]": !field.value,
      "max-h-[600px]": field.value,
    }
  );

  return <div className={completedByClasses}>{children}</div>;
};

export default CompletedByForm;
