import { Field, Form, Formik } from "formik";
import { generatePath, useNavigate, useSearchParams } from "react-router-dom";
import home from "../../assets/home2.png";
import amcLogo from "../../assets/AMC-logo.png";
import Input from "../../components/Input";
import { Registration } from "../../const/guestFieldMapping";
import CompletedByForm from "./CompletedByForm";
import * as Yup from "yup";
import usePeformScript from "../../api/usePerformScript";
import { CREATE_GUEST_SCRIPT, DEFAULT_WAVE } from "../../const/const";
import PersonalDetailsModule from "../../modules/PersonalDetails";
import { toast } from "react-toastify";

const initialValues = {
  [Registration.CompletedOther]: false,
  [Registration.CompletedByName]: "",
  [Registration.CompletedPhone]: "",
  [Registration.CompletedEmail]: "",
  [Registration.GuestType]: DEFAULT_WAVE,
};

const completedBySchema = Yup.object().shape({
  [Registration.CompletedOther]: Yup.boolean(),
  [Registration.CompletedByName]: Yup.string().when(
    Registration.CompletedOther,
    {
      is: true,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.CompletedPhone]: Yup.string().when(
    Registration.CompletedOther,
    {
      is: true,
      then: Yup.string().required("Required"),
    }
  ),
  [Registration.CompletedEmail]: Yup.string()
    .email("Must be a valid email")
    .when(Registration.CompletedOther, {
      is: true,
      then: Yup.string().email().required("Required"),
    }),
});

const Home = () => {
  const createGuest = usePeformScript<{ guestId: string }>(CREATE_GUEST_SCRIPT);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  const additionalData: Record<string, string> = {};

  searchParams.forEach((value, key) => {
    additionalData[key] = value;
  });

  const guestType = searchParams.get(Registration.GuestType) ?? "";
  console.log(guestType);

  return (
    <div className="flex">
      <div className="hidden flex-shrink md:block">
        <img src={home} className="h-screen object-cover" />
      </div>
      <div className="flex h-screen flex-grow items-center overflow-auto p-8">
        <div className="w-full ">
          <img src={amcLogo} alt="AMC Networks" className="pb-8" />
          <h1 className="pb-4 text-[40px] font-bold uppercase leading-none lg:text-[80px]">
            Arizona
            <br />
            Retreat
          </h1>
          <h2 className="pb-4 text-gray-700 sm:text-xl">
            So glad you’re joining us for an adventure filled weekend in the
            desert!
          </h2>
          <Formik
            initialValues={initialValues}
            validationSchema={completedBySchema}
            onSubmit={async (values) => {
              createGuest.mutate(
                { ...values, ...additionalData },
                {
                  onError: (err) => {
                    console.log(err);
                    toast.error("Error: Could not create guest record");
                  },
                  onSuccess: (data) => {
                    navigate(generatePath(PersonalDetailsModule.to, data));
                  },
                }
              );
            }}
          >
            <Form>
              <label className="flex items-baseline space-x-2 pb-2">
                <Field
                  type="checkbox"
                  className="relative top-[5px] h-5 w-5 accent-brand"
                  name={Registration.CompletedOther}
                />
                <span className="text-sm hover:cursor-pointer hover:underline">
                  Check if the registration form is to be completed by someone
                  other than the attending guest
                </span>
              </label>
              <CompletedByForm>
                <div className="my-4 bg-gray-50 p-4 shadow-inner">
                  <h2 className="text-lg">Completed By</h2>
                  <Input name={Registration.CompletedByName} label="Name" />
                  <Input
                    name={Registration.CompletedPhone}
                    label="Phone Number"
                  />
                  <Input name={Registration.CompletedEmail} label="Email" />
                </div>
              </CompletedByForm>

              {guestType.toLowerCase() !== "staff" && (
                <p className="mt-2 text-sm text-red-600">
                  * Must provide proof of full COVID-19 vaccination (inclusive
                  of booster shot, if eligible) in order to attend
                </p>
              )}

              <button
                className="btn btn-brand mt-4"
                type="submit"
                disabled={createGuest.isLoading}
              >
                {createGuest.isLoading ? "Processing..." : "Start Registration"}
              </button>
            </Form>
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default Home;
