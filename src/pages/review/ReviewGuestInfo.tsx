import useGuest from "../../api/useGuest";
import { Guest } from "../../const/guestFieldMapping";
import { getGuestFlow } from "../../modules";

interface Props {
  id: string;
}

const ReviewGuestInfo: React.FC<Props> = ({ id }) => {
  const guestQuery = useGuest(id);
  const flow = getGuestFlow(guestQuery.data);
  return (
    <div className="break-after-page">
      <h2 className="mt-4  pb-2 text-lg font-bold ">
        {guestQuery.data?.[Guest.NameFirstLast]}
      </h2>
      <div className="my-2 grid gap-2 lg:grid-cols-2">
        {flow.map(({ review, to }) => (
          <div key={to} className="span sm:border sm:p-6 sm:shadow">
            {review(id)}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ReviewGuestInfo;
