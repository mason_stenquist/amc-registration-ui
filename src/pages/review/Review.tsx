import { generatePath, Link, useNavigate, useParams } from "react-router-dom";
import useParty from "../../api/useParty";
import usePeformScript from "../../api/usePerformScript";
import { CONFIRMATION_EMAIL_SCRIPT } from "../../const/const";
import { Party } from "../../const/partyFieldMapping";
import ReviewGuestInfo from "./ReviewGuestInfo";

const Review = () => {
  const partyQuery = useParty();
  const params = useParams();
  const navigate = useNavigate();
  const sendConfirmation = usePeformScript(CONFIRMATION_EMAIL_SCRIPT);
  const handleClick = () => {
    sendConfirmation.mutate(
      { guestId: params?.partyId },
      {
        onSuccess: (data) => {
          navigate(generatePath("/complete/:partyId", params));
        },
      }
    );
  };
  return (
    <div className="-mx-4 border-t p-4 print:border-t-0 md:-mx-8 md:p-8">
      <h1 className="font-bold print:hidden md:text-2xl">
        Please review that all information is correct
      </h1>
      {(partyQuery.data || []).map((partyMember) => (
        <ReviewGuestInfo
          key={partyMember[Party.Zuuid]}
          id={partyMember[Party.Zuuid]}
        />
      ))}
      <div className="flex justify-end">
        <button
          onClick={handleClick}
          className="btn btn-brand"
          disabled={sendConfirmation.isLoading}
        >
          Submit Application
        </button>
      </div>
    </div>
  );
};

export default Review;
