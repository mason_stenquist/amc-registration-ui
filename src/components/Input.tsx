import { Field, FieldAttributes, useField } from "formik";
import classNames from "classnames";
import { InputHTMLAttributes, useEffect } from "react";
import { dateConvertFmToJs, isFmDate } from "../utils/dateUtils";

interface Props
  extends Omit<InputHTMLAttributes<HTMLInputElement>, "name" | "type"> {
  name: string;
  label?: string;
  type?: string;
  optional?: boolean;
  darkMode?: boolean;
}

const Input: React.FC<Props> = ({
  name,
  label,
  type,
  optional,
  darkMode,
  ...rest
}) => {
  const [{ value }, meta, helpers] = useField(name);

  useEffect(() => {
    if (type === "date") {
      if (isFmDate(value)) {
        helpers.setValue(dateConvertFmToJs(value));
      }
    }
  }, [value, type]);

  return (
    <Field name={name}>
      {({ field, form: { touched, errors } }: FieldAttributes<any>) => {
        const displayError = touched[name] && errors[name];
        const labelClasses = classNames("left-0 text-gray-600 text-sm", {
          "text-gray-200": darkMode,
        });
        const inputClasses = classNames(
          "bg-transparent peer h-10 w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-brand placeholder:text-slate-400 disabled:bg-gray-100 disabled:pl-1 disabled:placeholder:text-black",
          {
            "border-rose-500 focus:border-rose-500": displayError,
          },
          {
            "text-white  focus:border-white selection:text-black": darkMode,
          }
        );
        const errorClasses = classNames("text-sm text-red-600", {
          "text-rose-500 text-shadow": darkMode,
        });
        return (
          <div className="">
            {label && (
              <label htmlFor={name} className={labelClasses}>
                {label}{" "}
                {optional && (
                  <span className="text-xs opacity-60">(Optional)</span>
                )}
              </label>
            )}
            <input
              id={name}
              className={inputClasses}
              type={type || "text"}
              {...field}
              {...rest}
            />
            <div className="h-5 text-right">
              {displayError && (
                <div className={errorClasses}>{errors[name]}</div>
              )}
            </div>
          </div>
        );
      }}
    </Field>
  );
};

export default Input;
