import { generatePath, NavLink } from "react-router-dom";
import StatusChip from "./StatusChip";
import useRegistrationStatus from "../modules/hooks/useRegistrationStatus";

interface Props {
  guestId?: string;
}

const ModuleStatusLinks: React.FC<Props> = ({ guestId }) => {
  const registrationStatus = useRegistrationStatus(guestId);

  return (
    <ul className="text-sm">
      {registrationStatus.map(({ title, isValid, to }) => (
        <li key={to} className="h-7">
          <StatusChip isComplete={isValid} />{" "}
          <NavLink
            className={({ isActive }) =>
              isActive ? "underline" : "hover:underline"
            }
            to={generatePath(to, { guestId })}
          >
            {title}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};

export default ModuleStatusLinks;
