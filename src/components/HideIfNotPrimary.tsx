import React from "react";
import { Guest, GuestLayoutFields } from "../const/guestFieldMapping";

interface Props {
  guest: GuestLayoutFields | undefined;
  children: JSX.Element | JSX.Element[];
}

const ShowOnlyOnPrimary: React.FC<Props> = ({ guest, children }) => {
  if (guest?.[Guest.ZPartyID] != guest?.[Guest.Zuuid]) return null;
  return <>{children}</>;
};

export default ShowOnlyOnPrimary;
