import { generatePath, Link, useLocation } from "react-router-dom";
import { Module } from "../modules";

interface Props {
  guestId: string;
  module: Module;
}
const ReviewHeader: React.FC<Props> = ({ guestId, module }) => {
  const { pathname } = useLocation();
  return (
    <div className="mb-2 flex justify-between">
      <h3 className="font-bold">{module.title}</h3>
      <Link
        to={`${generatePath(module.to, { guestId })}?return=${pathname}`}
        className="text-sm text-brand underline"
      >
        Edit
      </Link>
    </div>
  );
};
export default ReviewHeader;
