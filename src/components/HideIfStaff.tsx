import React from "react";
import { GuestLayoutFields, Registration } from "../const/guestFieldMapping";

interface Props {
  guest: GuestLayoutFields | undefined;
  children: JSX.Element | JSX.Element[];
}

const HideIfStaff: React.FC<Props> = ({ guest, children }) => {
  if (guest?.[Registration.GuestType].toLowerCase() === "staff") return null;
  return <>{children}</>;
};

export default HideIfStaff;
