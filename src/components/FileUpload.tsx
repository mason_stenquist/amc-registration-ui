import classNames from "classnames";
import { useField } from "formik";
import { useDropzone } from "react-dropzone";
import { humanFileSize } from "../utils/humanFileSize";

interface Props {
  name: string;
  label: string;
  darkMode?: boolean;
}

const FileUpload: React.FC<Props> = ({ name, label, darkMode }) => {
  const [_field, meta, helpers] = useField(name);
  const { value, touched, error } = meta;
  const { setValue, setTouched } = helpers;
  const displayError = touched && error;

  //const [value, setValue] = useState<any>([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    multiple: false,
    onDrop: (acceptedFiles) => {
      const files = acceptedFiles.map((file) =>
        Object.assign(file, {
          url: URL.createObjectURL(file),
        })
      );
      setValue(files);
      setTouched(true);
    },
  });

  const handleDeleteClick = () => {
    setValue([]);
    setTouched(true);
  };

  const errorClasses = classNames("text-sm text-red-600", {
    "text-rose-500 text-shadow": darkMode,
  });

  const thumbs = value.map((file: any) => (
    <div className="" key={file.name}>
      <div className="border bg-gray-50 sm:flex sm:space-x-2">
        <img
          src={file.url}
          className="min-h-[45px] border-r object-cover sm:w-1/3"
        />
        <div className="py-1">
          <p className="text-sm font-bold">{file.name}</p>
          <p className="text-sm">{humanFileSize(file.size)}</p>
          <button
            onClick={handleDeleteClick}
            className="text-sm text-red-600 hover:text-red-700 hover:underline"
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  ));

  return (
    <section className="w-full">
      <h2 className="mb-2 text-sm text-gray-600">{label}</h2>
      {value.length === 0 && (
        <>
          <div
            {...getRootProps({
              className:
                "border-2 border-dashed w-full flex justify-center items-center p-6 text-center",
            })}
          >
            <input {...getInputProps()} />
            <p className="cursor-pointer hover:underline">
              Drag 'n' drop a file here, or click to select a file
            </p>
          </div>
          <div className="h-5 text-right">
            {displayError && <div className={errorClasses}>{error}</div>}
          </div>
        </>
      )}

      <aside>{thumbs}</aside>
    </section>
  );
};

export default FileUpload;
