interface Props {
  label: string;
}
const ReviewRow: React.FC<Props> = ({ label, children }) => {
  return (
    <tr>
      <td className="hidden pt-1 pr-3 align-top text-xs text-gray-500 sm:table-cell sm:w-[140px]">
        {label}
      </td>
      <td className="py-1 align-top text-xs">
        <p className="block sm:hidden">{label}:</p>
        <div>
          {children === "" ? (
            <span className="text-gray-300">N/A</span>
          ) : (
            children
          )}
        </div>
      </td>
    </tr>
  );
};
export default ReviewRow;
