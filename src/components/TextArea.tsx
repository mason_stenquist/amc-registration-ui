import { Field, FieldAttributes } from "formik";
import classNames from "classnames";
import { TextareaHTMLAttributes } from "react";

interface Props
  extends Omit<TextareaHTMLAttributes<HTMLTextAreaElement>, "name"> {
  name: string;
  label?: string;
  optional?: boolean;
  darkMode?: boolean;
}

const TextArea: React.FC<Props> = ({
  name,
  label,
  optional,
  darkMode,
  ...rest
}) => {
  return (
    <Field name={name}>
      {({ field, form: { touched, errors } }: FieldAttributes<any>) => {
        const displayError = touched[name] && errors[name];
        const labelClasses = classNames("left-0 text-gray-600 text-sm", {
          "text-gray-200": darkMode,
        });
        const inputClasses = classNames(
          "bg-transparent peer w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-brand placeholder:text-slate-400 disabled:bg-gray-100 disabled:pl-1 disabled:placeholder:text-black",
          {
            "border-rose-500 focus:border-rose-500": displayError,
          },
          {
            "text-white  focus:border-white selection:text-black": darkMode,
          }
        );
        const errorClasses = classNames("text-sm text-red-600", {
          "text-rose-500 text-shadow": darkMode,
        });
        return (
          <div className="">
            {label && (
              <label htmlFor={name} className={labelClasses}>
                {label}{" "}
                {optional && (
                  <span className="text-xs opacity-60">(Optional)</span>
                )}
              </label>
            )}
            <textarea
              id={name}
              className={inputClasses}
              {...field}
              {...rest}
            ></textarea>
            <div className="h-5 text-right">
              {displayError && (
                <div className={errorClasses}>{errors[name]}</div>
              )}
            </div>
          </div>
        );
      }}
    </Field>
  );
};

export default TextArea;
