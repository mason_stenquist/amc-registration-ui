import { generatePath, useNavigate, useParams } from "react-router-dom";
import { getNextModule, getPrevModule, Module } from "../modules";
import { useFormikContext } from "formik";
import { toast } from "react-toastify";
import { useMemo } from "react";
import useGuest from "../api/useGuest";
import { Guest } from "../const/guestFieldMapping";

interface Props {
  currentModule: Module;
  mutationStatus?: "error" | "idle" | "loading" | "success";
}

const NextPrevNavigation: React.FC<Props> = ({
  currentModule,
  mutationStatus,
}) => {
  const params = useParams();
  const guestQuery = useGuest();
  const navigate = useNavigate();
  const prevModule = getPrevModule(guestQuery?.data, currentModule);
  const nextModule = getNextModule(guestQuery?.data, currentModule);

  const nextButtonText = useMemo(() => {
    if (mutationStatus === "error") return "Error, Try again";
    if (mutationStatus === "loading") return "Saving Data";
    return nextModule ? "Next" : "Save";
  }, [mutationStatus]);

  const { touched, errors } = useFormikContext();

  const setToModuleAndSubmit = (to?: string) => {
    const touchedArray = Object.keys(touched);
    const errorsArray = Object.keys(errors);

    const location = to
      ? generatePath(to, params)
      : generatePath("/overview/:partyId", {
          partyId: guestQuery?.data?.[Guest.ZPartyID],
        });

    //If form has been not been touched, navigate without submitting
    if (touchedArray.length === 0) {
      navigate(location);
      return;
    }
    if (errorsArray.length !== 0) {
      toast.warn("Please fill in all required fields before continuing");
    }
  };

  return (
    <div className="flex justify-between pt-4">
      <div>
        {prevModule && (
          <button
            onClick={() => setToModuleAndSubmit(prevModule.to)}
            className=" btn btn-default"
          >
            Prev
          </button>
        )}
      </div>
      <div>
        <button
          type="submit"
          onClick={() => setToModuleAndSubmit(nextModule?.to)}
          className="btn btn-brand"
          disabled={mutationStatus === "loading"}
        >
          {nextButtonText}
        </button>
      </div>
    </div>
  );
};

export default NextPrevNavigation;
