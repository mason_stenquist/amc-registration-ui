import classNames from "classnames";

interface Props {
  isComplete: boolean;
}

const StatusChip: React.FC<Props> = ({ isComplete }) => {
  const statusClasses = classNames(
    "text-center pt-[1px] inline-block w-5 h-5 rounded-full text-xs font-bold ",
    {
      " text-emerald-600 ": isComplete,
    },
    {
      " text-rose-600 ": !isComplete,
    }
  );

  return <span className={statusClasses}>{isComplete ? "✓" : "✗"}</span>;
};

export default StatusChip;
